package mc.bloostrynetwork.antox11200.items;

import org.bukkit.entity.Player;

public interface ItemHandler {

	public abstract void handle(Player player);
	
}
