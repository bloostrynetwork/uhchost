package mc.bloostrynetwork.antox11200.items;

import java.util.List;
import java.util.Map;

import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;
import org.bukkit.inventory.meta.ItemMeta;

public class Item {
	
	private ItemStack item;
	private ItemMeta itemMeta;
	private ItemHandler itemHandler;

	public Item(Material material, String name, List<String> lore, ItemHandler itemHandler){
		this.item = new ItemStack(material);
		this.itemMeta = item.getItemMeta();
		this.itemMeta.setDisplayName(name);
		this.itemMeta.setLore(lore);
		this.item.setItemMeta(itemMeta);
		this.itemHandler = itemHandler;
	}

	public Item(Material material, Short durability, String name, List<String> lore, ItemHandler itemHandler){
		this.item = new ItemStack(material);
		this.item.setDurability(durability);
		this.itemMeta = item.getItemMeta();
		this.itemMeta.setDisplayName(name);
		this.itemMeta.setLore(lore);
		this.item.setItemMeta(itemMeta);
		this.itemHandler = itemHandler;
	}

	public Item(Material material, String name, List<String> lore, DyeColor dyeColor, ItemHandler itemHandler){
		this.item = new ItemStack(material);
		this.itemMeta = item.getItemMeta();
		((BannerMeta) this.itemMeta).setBaseColor(dyeColor);
		this.itemMeta.setDisplayName(name);
		this.itemMeta.setLore(lore);
		this.item.setItemMeta(itemMeta);
		this.itemHandler = itemHandler;
	}
	
	/* --- --- --- ---*/

	public Item(Material material, int amount,String name, List<String> lore, ItemHandler itemHandler){
		this.item = new ItemStack(material, amount);
		this.itemMeta = item.getItemMeta();
		this.itemMeta.setDisplayName(name);
		this.itemMeta.setLore(lore);
		this.item.setItemMeta(itemMeta);
		this.itemHandler = itemHandler;
	}

	public Item(Material material, int amount, Short durability, String name, List<String> lore, ItemHandler itemHandler){
		this.item = new ItemStack(material, amount);
		this.item.setDurability(durability);
		this.itemMeta = item.getItemMeta();
		this.itemMeta.setDisplayName(name);
		this.itemMeta.setLore(lore);
		this.item.setItemMeta(itemMeta);
		this.itemHandler = itemHandler;
	}

	public Item(Material material, int amount,String name,  List<String> lore, DyeColor dyeColor, ItemHandler itemHandler){
		this.item = new ItemStack(material, amount);
		this.itemMeta = item.getItemMeta();
		((BannerMeta) this.itemMeta).setBaseColor(dyeColor);
		this.itemMeta.setDisplayName(name);
		this.itemMeta.setLore(lore);
		this.item.setItemMeta(itemMeta);
		this.itemHandler = itemHandler;
	}
	
	public void setName(String name){
		this.itemMeta.setDisplayName(name);
	}
	
	public String getName(){
		return this.itemMeta.getDisplayName();
	}
	
	public void setLore(List<String> lore){
		this.itemMeta.setLore(lore);
	}
	
	public List<String> getLore(){
		return this.itemMeta.getLore();
	}

	public Map<Enchantment, Integer> getEnchantements(){
		return this.itemMeta.getEnchants();
	}
	
	public void addEnchantment(Enchantment enchantment, Integer level, Boolean result){
		this.itemMeta.addEnchant(enchantment, level, result);
	}
	
	public void removeEnchantment(Enchantment enchantment){
		this.itemMeta.removeEnchant(enchantment);
	}
	
	public Boolean hasEnchantment(Enchantment enchantment){
		return this.hasEnchantment(enchantment);
	}
	
	public ItemStack toItemStack(){
		return this.item;
	}

	public void setDurability(short s) {
		this.item.setDurability(s);
	}

	public ItemHandler getItemHandler() {
		return itemHandler;
	}

	public void setItemHandler(ItemHandler itemHandler) {
		this.itemHandler = itemHandler;
	}

	public void handle(Player player) {
		if(itemHandler == null)return;
		itemHandler.handle(player);
	}
}
