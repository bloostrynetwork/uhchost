package mc.bloostrynetwork.antox11200.player;

import org.bukkit.entity.Player;

public class Kill {

	public Player killer;
	public Player victime;
	public String reason;
	public Long killedTime;
	
	public Kill(Player killer, Player victime, String reason) {
		this.killer = killer;
		this.victime = victime;
		this.reason = reason;
		this.killedTime = System.currentTimeMillis();
	}
}
