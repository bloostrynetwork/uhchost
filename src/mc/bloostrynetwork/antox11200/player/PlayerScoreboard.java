package mc.bloostrynetwork.antox11200.player;

import java.text.DecimalFormat;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.Location;

import mc.bloostrynetwork.antox11200.bloostryapi.utils.DateUtils;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.SimpleScoreboard;
import mc.bloostrynetwork.antox11200.manager.game.GameManager;
import mc.bloostrynetwork.antox11200.manager.game.GameState;
import mc.bloostrynetwork.antox11200.manager.game.Step;
import mc.bloostrynetwork.antox11200.manager.game.scenarios.depends.Scenarios;
import mc.bloostrynetwork.antox11200.utils.DirectionUtils;

public class PlayerScoreboard {

	private UPlayer uplayer;
	private HashMap<Integer, String> lines = new HashMap<>();
	private SimpleScoreboard simpleScoreboard;

	public boolean refresh = false;

	public static final String TITLE = "§6BloostryNetwork";

	public PlayerScoreboard(UPlayer uplayer) {
		this.uplayer = uplayer;
		create();
	}

	public void create() {
		simpleScoreboard = new SimpleScoreboard(TITLE + " §8| §e#"+GameManager.getHostNumber());
		simpleScoreboard.send(uplayer.getPlayer());
		refresh = true;
	}

	public void delete() {
		this.simpleScoreboard.delete();
		this.simpleScoreboard = null;
	}

	public UPlayer getUPlayer() {
		return this.uplayer;
	}
	
	public SimpleScoreboard getSimpleScoreboard() {
		return this.simpleScoreboard;
	}
	
	public void refresh() {
		if (!(refresh))
			return;
		if (this.simpleScoreboard == null)
			create();
		
		if(!(uplayer.isOnline()))
			return;

		Step stade = GameManager.gameState.stade;
		if (stade == Step.INWAITING) {
			lines.put(15, "§7§m-------------------");
			lines.put(14, "§6➡ Joueurs: §e" + Bukkit.getOnlinePlayers().size()+"§8/§6"+GameManager.MAX_PLAYERS);
			lines.put(13, "§6➡ §e" + GameManager.teamType.name);
			lines.put(12, "§8§7§m-------------------");
			int max = 11;
			for (Scenarios scenarios : Scenarios.values()) {
				if (max <= 2) {
					break;
				}
				if (scenarios.iScenario.isEnabled()) {
					lines.put(max, "§7- §e" + scenarios.iScenario.getName());
				}
				max--;
			}
			if(max == 11) {
				lines.put(11, "§cAucun scénarios activée");
			}
		} else if (GameManager.gameState == GameState.FINISH) {
			lines.put(15, "§7§m-------------------");
			lines.put(14, "§7§5 ");
			lines.put(13, "§6Victoire "	+ (GameManager.hasTeam() ? "de l'équipe de: " : "de: "));
			
			if (GameManager.hasTeam()) {
				lines.put(12, GameManager.WINNER.team.getPrefix() + " " + GameManager.WINNER.team.getOwner().getName());

				int i = 11;
				for(String players : GameManager.WINNER.team.getPlayers()) {
					if(GameManager.players.get(players).dead) {
						lines.put(i, "§e- §C"+players);
					} else {
						lines.put(i, "§e- §a"+players);
					}
					i++;
				}
			} else {
				lines.put(12, "§e"+GameManager.WINNER.getName());
			}
			lines.put(4, "§8§9 ");
			lines.put(3, "§6Kill(s): §e" +uplayer.getKills().size());
		} else if (stade == Step.INGAME) {
			lines.put(15, "§7§m-------------------");
			lines.put(14, "§6Taille: §e-" + (((int) GameManager.BORDER_RUNNABLE.getSize() / 2)) + "§7/§e+"+ (((int) GameManager.BORDER_RUNNABLE.getSize() / 2)));
			lines.put(13,
					"§6Centre: §e" + new DecimalFormat("####").format(
							new Location(GameManager.getGameWorld(), 0, uplayer.getPlayer().getLocation().getBlockY(), 0)
									.distance(uplayer.getPlayer().getLocation()))+ " blocs §6"+DirectionUtils.getArrow(DirectionUtils.getAngle(this.uplayer.getPlayer(), GameManager.getSpawnLocation())));
			lines.put(12, "§c§7§m-------------------");
			if (GameManager.hasTeam())
				lines.put(11, "§6Équipes: §e" + GameManager.getTeamSize());
			else
				lines.put(11, "§6Joueurs: §e" + GameManager.playersInGame.size());
			lines.put(10, "§6Kill(s): §e" + getUPlayer().getKills().size());
			lines.put(9, "§8§7§m-------------------");
			lines.put(8, "§6Bordure: " +  (DateUtils.getTimerDate(GameManager.BORDER_RUNNABLE.getTimer()) == null ? "§e"+"✔" : "§e"+ DateUtils.getTimerDate(GameManager.BORDER_RUNNABLE.getTimer())));
			lines.put(7, "§6PvP: " + (DateUtils.getTimerDate(GameManager.PVP_RUNNABLE.getTimer()) == null ? "§e"+"✔" : "§e"+ DateUtils.getTimerDate(GameManager.PVP_RUNNABLE.getTimer())));
		}
		lines.put(2, "§f§7§m-------------------");
		lines.put(1, "§eplay.bloostrynetwork.fr");
		for (Integer slot : lines.keySet()) {
			String line = lines.get(slot);
			simpleScoreboard.add(line, slot);
		}

		for (int loc = 15; loc > 0; loc--) {
			String myLine = lines.get(loc);
			String sbLine = this.simpleScoreboard.get(loc);

			if (myLine == null && sbLine != null) {
				this.simpleScoreboard.remove(loc);
				this.simpleScoreboard.reset();
			} else {
				if (sbLine != null && myLine != null) {
					if (!sbLine.equals(myLine)) {
						this.simpleScoreboard.remove(loc, sbLine);
						this.simpleScoreboard.add(myLine, loc);
					}
				}
			}
		}
		simpleScoreboard.update();
		lines.clear();
	}
}
