package mc.bloostrynetwork.antox11200.player;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.potion.PotionEffect;

import mc.bloostrynetwork.antox11200.BloostryUHC;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.Title;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.player.BloostryPlayer;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.rank.Ranks;
import mc.bloostrynetwork.antox11200.manager.game.GameManager;
import mc.bloostrynetwork.antox11200.manager.game.GameState;
import mc.bloostrynetwork.antox11200.manager.game.scenarios.depends.Scenarios;
import mc.bloostrynetwork.antox11200.manager.game.team.Team;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;

public class UPlayer {

	private String name;
	private UUID uuid;
	private BloostryPlayer iplayer;
	private Player player;
	private ArrayList<Kill> kills = new ArrayList<>();

	public boolean connected = false;
	public boolean specMode = false;
	public boolean dead = false;

	public PlayerScoreboard playerScoreboard;

	public ItemStack[] lastEquipments;
	public ItemStack[] lastInventory;
	public Location lastLocation;

	public Location spawnLocation;
	public ItemStack[] spawnEquipments;
	public ItemStack[] spawnInventory;

	public boolean revive = false;

	public Team team = null;

	public UPlayer(String name, UUID uuid, BloostryPlayer iplayer) {
		this.name = name;
		this.player = Bukkit.getPlayer(name);
		this.uuid = uuid;
		this.iplayer = iplayer;
	}

	public void joinInWaiting() {
		Player player = getPlayer();
		clearArmor();
		clearInventory();
		player.setHealth(20.0);
		player.setFoodLevel(20);
		player.setLevel(0);
		player.setWalkSpeed(0.2F);
		player.setExp(0F);
		player.setGameMode(GameMode.SURVIVAL);
		for (PotionEffect potions : player.getActivePotionEffects()) {
			if (potions != null) {
				player.removePotionEffect(potions.getType());
			}
		}
		player.teleport(GameManager.getLobbyLocation());
		connected = true;
		if (iplayer.getBloostryRank().getPermissionPower() >= Ranks.HOST.getBloostryRank().getPermissionPower() && GameManager.gameState == GameState.HOST) {
			player.sendMessage(GameManager.PREFIX + "§6Veuillez configurer la partie.");
		}
	}

	public void clearArmor() {
		getPlayer().getInventory().setArmorContents(null);
	}

	public void clearInventory() {
		getPlayer().getInventory().clear();
	}

	public Player getPlayer() {
		return Bukkit.getPlayer(name);
	}

	public boolean isOnline() {
		return getPlayer() != null && getPlayer().isOnline();
	}

	public String getName() {
		return name;
	}

	public UUID getUuid() {
		return uuid;
	}

	public BloostryPlayer getIPlayer() {
		return iplayer;
	}

	public void setIPlayer(BloostryPlayer iplayer) {
		this.iplayer = iplayer;
	}

	public void sendTitle(String message, String subMessage, Integer seconds) {
		Title title = new Title(message, subMessage, 1, seconds, 1);
		title.send(this.player);
	}

	public void sendActionBar(String text) {
		if (!(isOnline()))
			return;
		final String newText = text.replace("_", " ");
		String s = ChatColor.translateAlternateColorCodes('&', newText);
		IChatBaseComponent icbc = ChatSerializer.a("{\"text\": \"" + s + "\"}");
		PacketPlayOutChat bar = new PacketPlayOutChat(icbc, (byte) 2);
		((CraftPlayer) getPlayer()).getHandle().playerConnection.sendPacket(bar);
	}

	public void revive() {
		if (dead) {
			if (specMode)
				specMode = false;
			dead = false;
			if (isOnline()) {
				Player player = getPlayer();
				player.teleport(lastLocation);
				player.getInventory().setArmorContents(lastEquipments);
				player.getInventory().setContents(lastInventory);
				
				player.setGameMode(GameMode.SURVIVAL);
				
				player.sendMessage(GameManager.PREFIX+"§aVous avez résister.");
				
				GameManager.playersInGame.add(name);
			} else {
				spawnLocation = lastLocation;
				spawnEquipments = lastEquipments;
				spawnInventory = lastInventory;
				revive = true;
			}
			if (team != null) {
				team.addPlayer(name);
				team.removePlayerDeath(name);
			}
		}
	}

	public void death(String reason) {
		if (GameManager.gameState == GameState.FINISH)
			return;
		if (specMode)
			return;
		if (player.isOnline()) {

			lastLocation = player.getLocation();
			lastEquipments = player.getInventory().getArmorContents();
			lastInventory = player.getInventory().getContents();

			if (!Scenarios.TIMEBOMB.iScenario.isEnabled()) {
				for (ItemStack items : player.getInventory().getContents()) {
					if (items != null && items.getType() != Material.AIR)
						player.getWorld().dropItem(player.getLocation(), items);
				}
				for (ItemStack items : player.getInventory().getArmorContents()) {
					if (items != null && items.getType() != Material.AIR)
						player.getWorld().dropItem(player.getLocation(), items);
				}

				ItemStack skullVictim = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
				SkullMeta skullM = (SkullMeta) skullVictim.getItemMeta();
				skullM.setDisplayName("§6Tête de §e" + player.getName());
				skullM.setOwner(player.getName());
				skullVictim.setItemMeta(skullM);

				player.getWorld().dropItemNaturally(player.getLocation(), skullVictim);
			}
		}

		if (team != null) {
			team.removePlayer(name);
			team.addPlayerDeath(name);
		}

		dead = true;

		GameManager.playersInGame.remove(getPlayer().getName());

		if (GameManager.hasTeam())
			reason += "§8(§6Équipes: §e" + GameManager.getTeamSize() + "§8)§6.";
		else
			reason += "§8(§6Joueurs: §e" + GameManager.playersInGame.size() + "§8)§6.";

		if (GameManager.hasTeam()) {
			if (GameManager.getTeamSize() == 1) {
				GameManager.gameFinish(GameManager.playersInGame.get(0));
			}
		} else {
			if (GameManager.playersInGame.size() == 1) {
				GameManager.gameFinish(GameManager.playersInGame.get(0));
			}
		}

		if (player.isOnline()) {
			player.setGameMode(GameMode.SPECTATOR);
		}

		sendTitle("§c✖ Vous êtes mort ✖", "§eVous serrez éjecté du serveur dans §610 §esecondes !", 2);

		Bukkit.broadcastMessage(GameManager.PREFIX + reason);
		Bukkit.getScheduler().runTaskLater(BloostryUHC.getInstance(), new Runnable() {
			@Override
			public void run() {
				if (!(player.isOnline()))
					return;
				if(!(dead))return;
				player.kickPlayer("§c✖ Vous êtes mort ✖ \n" + "§eMerci d'avoir joué sur notre serveur");
			}
		}, 10 * 20);
	}

	public void addKill(Kill kill) {
		kills.add(kill);
	}

	public List<Kill> getKills() {
		return kills;
	}

	public void joinInStaff() {
		Player player = getPlayer();
		this.specMode = true;
		if (GameManager.gameState == GameState.FINISH) {
			player.teleport(GameManager.getLobbyLocation());
		} else {
			player.teleport(GameManager.getSpawnLocation());
		}
		player.setGameMode(GameMode.SPECTATOR);
		player.setWalkSpeed(0.2F);
		player.setHealth(20.0);
		player.setFoodLevel(20);
		player.setLevel(0);
		player.setExp(0F);
		player.sendMessage(GameManager.PREFIX + "§6Vous êtes spectateur !");
	}

	public void setPlayer(Player player) {
		this.player = player;
	}
}
