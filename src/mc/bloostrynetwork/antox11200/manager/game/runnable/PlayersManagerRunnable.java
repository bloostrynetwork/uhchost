	package mc.bloostrynetwork.antox11200.manager.game.runnable;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;

import mc.bloostrynetwork.antox11200.BloostryUHC;
import mc.bloostrynetwork.antox11200.bloostryapi.BloostryAPI;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.TabAPI;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.player.BloostryPlayer;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.rank.Ranks;
import mc.bloostrynetwork.antox11200.manager.game.GameManager;
import mc.bloostrynetwork.antox11200.manager.game.Step;
import mc.bloostrynetwork.antox11200.player.UPlayer;

public class PlayersManagerRunnable implements Runnable {

	private BukkitTask task;

	public void start() {
		this.task = Bukkit.getScheduler().runTaskTimer(BloostryUHC.getInstance(), this, 0, 4);
	}

	public BukkitTask getTask() {
		return task;
	}

	public void cancel() {
		task.cancel();
	}

	@Override
	public void run() {
		for (String playerName : GameManager.players.keySet()) {
			UPlayer uplayer = GameManager.players.get(playerName);
			Player player = Bukkit.getPlayer(playerName);
			if (player != null) {
				uplayer.setPlayer(player);
				
				uplayer.playerScoreboard.refresh();
				
				String header = "\n§6             §eplay.bloostrynetwork.fr§6              \n";

				String footer = "\n§6Joueurs: §e"
						+ (GameManager.gameState.stade == Step.INGAME ? GameManager.playersInGame.size()
								: Bukkit.getOnlinePlayers().size())
						+ "§7/§e" + GameManager.MAX_PLAYERS + "";

				TabAPI.setPlayerList(uplayer.getPlayer(), header, footer);

				BloostryPlayer iplayer = BloostryAPI.getBloostryPlayer(player.getUniqueId());

				Objective objective = uplayer.playerScoreboard.getSimpleScoreboard().getScoreboard()
						.getObjective("showhealth");
				
				if (objective == null) {
					objective = uplayer.playerScoreboard.getSimpleScoreboard().getScoreboard()
							.registerNewObjective("showhealth", "health");

					objective.setDisplaySlot(DisplaySlot.BELOW_NAME);
					objective.setDisplayName("§C"+"❤");
				}

				for (Player online : Bukkit.getOnlinePlayers()) {
					online.setHealth(online.getHealth()); // Update their health
				}

				if (uplayer.team != null) {
					String prefixTab = iplayer.getBloostryRank().getPrefix() + uplayer.team.getPrefix();
					String suffixTab = " §e" + ((int) uplayer.getPlayer().getHealth() * 5);

					player.setPlayerListName(prefixTab + " " + uplayer.getName() + suffixTab);

					for (Player players : Bukkit.getOnlinePlayers()) {
						BloostryPlayer iplayers = BloostryAPI.getBloostryPlayer(players.getUniqueId());
						OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(players.getName());
						for (Ranks rank : Ranks.values()) {
							org.bukkit.scoreboard.Team scoreboardTeam = uplayer.playerScoreboard.getSimpleScoreboard()
									.getScoreboard().getTeam(rank.getBloostryRank().getTeamName());
							if (scoreboardTeam == null) {
								uplayer.playerScoreboard.getSimpleScoreboard().getScoreboard()
										.registerNewTeam(rank.getBloostryRank().getTeamName());
								scoreboardTeam = uplayer.playerScoreboard.getSimpleScoreboard().getScoreboard()
										.getTeam(rank.getBloostryRank().getTeamName());
							}

							if (iplayers.getBloostryRank().getId() != rank.getBloostryRank().getId()) {
								if (scoreboardTeam.getPlayers().contains(offlinePlayer)) {
									scoreboardTeam.removePlayer(offlinePlayer);
								}
							} else {
								if (iplayers.getBloostryRank().getId() == rank.getBloostryRank().getId()) {
									if (!scoreboardTeam.getPlayers().contains(offlinePlayer)) {
										scoreboardTeam.addPlayer(offlinePlayer);
									}
								}
							}
						}
					}
				} else {
					String prefixTab = iplayer.getBloostryRank().getPrefix();
					String suffixTab = " §e" + ((int) uplayer.getPlayer().getHealth() * 5);

					player.setPlayerListName(prefixTab + uplayer.getName() + suffixTab);

					for (Player players : Bukkit.getOnlinePlayers()) {
						BloostryPlayer iplayers = BloostryAPI.getBloostryPlayer(players.getUniqueId());
						OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(players.getName());
						for (Ranks rank : Ranks.values()) {
							org.bukkit.scoreboard.Team scoreboardTeam = uplayer.playerScoreboard.getSimpleScoreboard()
									.getScoreboard().getTeam(rank.getBloostryRank().getTeamName());
							if (scoreboardTeam == null) {
								uplayer.playerScoreboard.getSimpleScoreboard().getScoreboard()
										.registerNewTeam(rank.getBloostryRank().getTeamName());
								scoreboardTeam = uplayer.playerScoreboard.getSimpleScoreboard().getScoreboard()
										.getTeam(rank.getBloostryRank().getTeamName());
							}

							if (iplayers.getBloostryRank().getId() != rank.getBloostryRank().getId()) {
								if (scoreboardTeam.getPlayers().contains(offlinePlayer)) {
									scoreboardTeam.removePlayer(offlinePlayer);
								}
							} else {
								if (iplayers.getBloostryRank().getId() == rank.getBloostryRank().getId()) {
									if (!scoreboardTeam.getPlayers().contains(offlinePlayer)) {
										scoreboardTeam.addPlayer(offlinePlayer);
									}
								}
							}
						}
					}
				}
			}
		}
	}
}
