package mc.bloostrynetwork.antox11200.manager.game.runnable;

import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitTask;

import mc.bloostrynetwork.antox11200.BloostryUHC;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.Title;
import mc.bloostrynetwork.antox11200.manager.game.GameManager;
import mc.bloostrynetwork.antox11200.manager.game.GameState;
import mc.bloostrynetwork.antox11200.player.UPlayer;

public class PvPRunnable implements Runnable {

	private BukkitTask task;

	public int timer = GameManager.PVP_MIN_TIME;
	private int maxTimer;

	public PvPRunnable(int maxTimer) {
		this.maxTimer = maxTimer;
	}
	
	public void start() {
		this.task = Bukkit.getScheduler().runTaskTimer(BloostryUHC.getInstance(), this, 0, 20);
	}

	public BukkitTask getTask() {
		return task;
	}

	public void setTask(BukkitTask task) {
		this.task = task;
	}

	public int getTimer() {
		return timer;
	}

	public void setTimer(int timer) {
		this.timer = timer;
	}

	public int getMaxTimer() {
		return maxTimer;
	}

	public void setMaxTimer(int maxTimer) {
		this.maxTimer = maxTimer;
	}

	public void cancel() {
		task.cancel();
	}

	@Override
	public void run() {
		timer--;
		switch (timer) {
		case (10 * 60):
			for (UPlayer uplayers : GameManager.players.values()) {
				if(uplayers.isOnline()) {
					uplayers.sendTitle("�6PvP actif dans", "�e"+(timer / 60)+" minutes", 3);
				}
			}
			break;
		case (5 * 60):
			for (UPlayer uplayers : GameManager.players.values()) {
				if(uplayers.isOnline()) {
					uplayers.sendTitle("�6PvP actif dans", "�e"+(timer / 60)+" minutes", 3);
				}
			}
			break;
		case (60):
			for (UPlayer uplayers : GameManager.players.values()) {
				if(uplayers.isOnline()) {
					uplayers.sendTitle("�6PvP actif dans", "�e"+(timer / 60)+" minute", 3);
				}
			}
			break;
		case (30):
			for (UPlayer uplayers : GameManager.players.values()) {
				if(uplayers.isOnline()) {
					uplayers.sendTitle("�6PvP actif dans", "�e"+timer+" secondes", 3);
				}
			}
			break;
		case (15):
			for (UPlayer uplayers : GameManager.players.values()) {
				if(uplayers.isOnline()) {
					uplayers.sendTitle("�6PvP actif dans", "�e"+timer+" secondes", 3);
				}
			}
			break;
		case (10):
			for (UPlayer uplayers : GameManager.players.values()) {
				if(uplayers.isOnline()) {
					uplayers.sendTitle("�6PvP actif dans", "�e"+timer+" secondes", 3);
				}
			}
			break;
		case (5):
			for (UPlayer uplayers : GameManager.players.values()) {
				if(uplayers.isOnline()) {
					uplayers.sendTitle("�6PvP actif dans", "�e"+timer+" secondes", 1);
				}
			}
			break;
		case (4):
			for (UPlayer uplayers : GameManager.players.values()) {
				if(uplayers.isOnline()) {
					uplayers.sendTitle("�6PvP actif dans", "�e"+timer+" secondes", 1);
				}
			}
			break;
		case (3):
			for (UPlayer uplayers : GameManager.players.values()) {
				if(uplayers.isOnline()) {
					uplayers.sendTitle("�6PvP actif dans", "�e"+timer+" secondes", 1);
				}
			}
			break;
		case (2):
			for (UPlayer uplayers : GameManager.players.values()) {
				if(uplayers.isOnline()) {
					uplayers.sendTitle("�6PvP actif dans", "�e"+timer+" secondes", 1);
				}
			}
			break;
		case (1):
			for (UPlayer uplayers : GameManager.players.values()) {
				if(uplayers.isOnline()) {
					uplayers.sendTitle("�6PvP actif dans", "�e"+timer+" secondes", 1);
				}
			}
			break;
		case (0):
		GameManager.gameState = GameState.PVP;
		for (UPlayer uplayers : GameManager.players.values()) {
				if(uplayers.connected) {
					GameManager.PVP = true;
					new Title("�6PvP activ�.", "�6", 1, 2, 1).send(uplayers.getPlayer());	
				} else {
					uplayers.death("�e"+uplayers.getName()+" �6est mort en d�connectant !");
				}
			}
			cancel();
			break;
		}
	}
}
