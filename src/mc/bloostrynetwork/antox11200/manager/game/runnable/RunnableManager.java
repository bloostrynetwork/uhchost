package mc.bloostrynetwork.antox11200.manager.game.runnable;

public class RunnableManager {

	public static PlayersManagerRunnable playersManagerRunnable;
	
	public void loadAllRunnable() {
		playersManagerRunnable = new PlayersManagerRunnable();
		playersManagerRunnable.start();
	}
	
	public void unloadAllRunnable() {
		playersManagerRunnable.cancel();
	}
}
