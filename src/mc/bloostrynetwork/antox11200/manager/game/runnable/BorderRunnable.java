package mc.bloostrynetwork.antox11200.manager.game.runnable;

import org.bukkit.Bukkit;
import org.bukkit.WorldBorder;
import org.bukkit.scheduler.BukkitTask;

import mc.bloostrynetwork.antox11200.BloostryUHC;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.Title;
import mc.bloostrynetwork.antox11200.manager.game.GameManager;
import mc.bloostrynetwork.antox11200.manager.game.GameState;
import mc.bloostrynetwork.antox11200.player.UPlayer;

public class BorderRunnable implements Runnable {

	private BukkitTask task;

	private WorldBorder worldBorder;

	public int timer = GameManager.BORDER_MIN_TIME * 2;
	private int maxTimer;

	public BorderRunnable(int maxTimer) {
		this.maxTimer = maxTimer;
		this.worldBorder = GameManager.getGameWorld().getWorldBorder();
		this.worldBorder.setSize(GameManager.BORDER_SIZE);
		this.worldBorder.setDamageAmount(2.0);
	}

	public void start() {
		this.task = Bukkit.getScheduler().runTaskTimer(BloostryUHC.getInstance(), this, 0, 20);
	}

	public BukkitTask getTask() {
		return task;
	}

	public void setTask(BukkitTask task) {
		this.task = task;
	}

	public int getTimer() {
		return timer;
	}

	public void setTimer(int timer) {
		this.timer = timer;
	}

	public int getMaxTimer() {
		return maxTimer;
	}

	public void setMaxTimer(int maxTimer) {
		this.maxTimer = maxTimer;
	}

	public void cancel() {
		task.cancel();
	}

	@Override
	public void run() {
		timer--;
		switch (timer) {
		case (10 * 60):
			for (UPlayer uplayers : GameManager.players.values()) {
				if (uplayers.isOnline()) {
					uplayers.sendTitle("�6Bordure actif dans", "�e" + (timer / 60) + " minutes", 3);
				}
			}
			break;
		case (5 * 60):
			for (UPlayer uplayers : GameManager.players.values()) {
				if (uplayers.isOnline()) {
					uplayers.sendTitle("�6Bordure actif dans", "�e" + (timer / 60) + " minutes", 3);
				}
			}
			break;
		case (60):
			for (UPlayer uplayers : GameManager.players.values()) {
				if (uplayers.isOnline()) {
					uplayers.sendTitle("�6Bordure actif dans", "�e" + (timer / 60) + " minute", 3);
				}
			}
			break;
		case (30):
			for (UPlayer uplayers : GameManager.players.values()) {
				if (uplayers.isOnline()) {
					uplayers.sendTitle("�6Bordure actif dans", "�e" + timer + " secondes", 3);
				}
			}
			break;
		case (15):
			for (UPlayer uplayers : GameManager.players.values()) {
				if (uplayers.isOnline()) {
					uplayers.sendTitle("�6Bordure actif dans", "�e" + timer + " secondes", 3);
				}
			}
			break;
		case (10):
			for (UPlayer uplayers : GameManager.players.values()) {
				if (uplayers.isOnline()) {
					uplayers.sendTitle("�6Bordure actif dans", "�e" + timer + " secondes", 3);
				}
			}
			break;
		case (5):
			for (UPlayer uplayers : GameManager.players.values()) {
				if (uplayers.isOnline()) {
					uplayers.sendTitle("�6Bordure actif dans", "�e" + timer + " secondes", 1);
				}
			}
			break;
		case (4):
			for (UPlayer uplayers : GameManager.players.values()) {
				if (uplayers.isOnline()) {
					uplayers.sendTitle("�6Bordure actif dans", "�e" + timer + " secondes", 1);
				}
			}
			break;
		case (3):
			for (UPlayer uplayers : GameManager.players.values()) {
				if (uplayers.isOnline()) {
					uplayers.sendTitle("�6Bordure actif dans", "�e" + timer + " secondes", 1);
				}
			}
			break;
		case (2):
			for (UPlayer uplayers : GameManager.players.values()) {
				if (uplayers.isOnline()) {
					uplayers.sendTitle("�6Bordure actif dans", "�e" + timer + " secondes", 1);
				}
			}
			break;
		case (1):
			for (UPlayer uplayers : GameManager.players.values()) {
				if (uplayers.isOnline()) {
					uplayers.sendTitle("�6Bordure actif dans", "�e" + timer + " secondes", 1);
				}
			}
			break;
		case (0):
			for (UPlayer uplayers : GameManager.players.values()) {
				if (uplayers.connected) {
					startMoveBorder();
					cancel();
					new Title("�6Bordure actif.", "�6", 1, 2, 1).send(uplayers.getPlayer());
				}
			}
			break;
		}
	}

	public void startMoveBorder() {
		GameManager.gameState = GameState.BORDER;
		GameManager.getGameWorld().getWorldBorder().setSize(GameManager.BORDER_SIZE);
		GameManager.getGameWorld().getWorldBorder().setSize(100, 700);
	}

	public WorldBorder getWorldBorder() {
		return GameManager.getGameWorld().getWorldBorder();
	}

	public double getSize() {
		return GameManager.getGameWorld().getWorldBorder().getSize();
	}
}
