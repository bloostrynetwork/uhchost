package mc.bloostrynetwork.antox11200.manager.game.scenarios;

import java.util.ArrayList;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.ExperienceOrb;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

import mc.bloostrynetwork.antox11200.manager.game.scenarios.depends.IScenario;
import mc.bloostrynetwork.antox11200.manager.game.scenarios.depends.Scenarios;

public class TripleOres implements IScenario {

	private boolean enabled = false;

	@Override
	public String getName() {
		return "TripleOres";
	}

	@Override
	public boolean isEnabled() {
		return this.enabled;
	}

	@Override
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	@Override
	public ItemStack getIcon() {
		return new ItemStack(Material.GOLD_INGOT, 3);
	}

	@Override
	public String getDescription() {
		return "�6Au minage les minerais sont tripl�s";// ',' retour � la ligne
	}

	@Override
	public ArrayList<String> getListener() {
		ArrayList<String> listListener = new ArrayList<>();
		listListener.add("BlockBreakEvent");
		return listListener;
	}

	@Override
	public void handle(String eventName, Object eventObject) {
		if (!(isEnabled()))
			return;
		if (eventName.equals("BlockBreakEvent")) {
			BlockBreakEvent e = (BlockBreakEvent) eventObject;
			Block b = e.getBlock();
			Material bt = e.getBlock().getType();
			Player p = e.getPlayer();
			if (!Scenarios.BAREBONES.iScenario.isEnabled()) {
				if (bt == Material.DIAMOND_ORE) {
					e.setCancelled(true);
					b.getLocation().getBlock().setType(Material.AIR);
					p.getWorld().dropItem(p.getLocation(), new ItemStack(Material.DIAMOND, 3));
					((ExperienceOrb) b.getWorld().spawn(b.getLocation(), ExperienceOrb.class)).setExperience(1);
				}
				if (bt == Material.GOLD_ORE) {
					e.setCancelled(true);
					b.getLocation().getBlock().setType(Material.AIR);
					p.getWorld().dropItem(p.getLocation(), new ItemStack(Material.GOLD_INGOT, 3));
					((ExperienceOrb) b.getWorld().spawn(b.getLocation(), ExperienceOrb.class)).setExperience(2);
				}
				if (bt == Material.IRON_ORE) {
					e.setCancelled(true);
					b.getLocation().getBlock().setType(Material.AIR);
					p.getWorld().dropItem(p.getLocation(), new ItemStack(Material.IRON_INGOT, 3));
					((ExperienceOrb) b.getWorld().spawn(b.getLocation(), ExperienceOrb.class)).setExperience(3);
				}
			}
		}
	}
}
