package mc.bloostrynetwork.antox11200.manager.game.scenarios.depends;

import java.util.ArrayList;

import org.bukkit.inventory.ItemStack;

public interface IScenario {

	public abstract String getName();
	public abstract boolean isEnabled();
	public abstract void setEnabled(boolean enabled);
	public abstract String getDescription();
	public abstract ItemStack getIcon();
	public abstract ArrayList<String> getListener();
	public abstract void handle(String eventName, Object eventObject);
	
}
