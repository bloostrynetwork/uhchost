package mc.bloostrynetwork.antox11200.manager.game.scenarios.depends;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.PlayerDeathEvent;

import mc.bloostrynetwork.antox11200.bloostryapi.utils.console.Console;

public class EventsListeners implements Listener {

	@EventHandler(priority = EventPriority.LOWEST)
	public void EntityDeathEvent_(EntityDeathEvent e) {
		if(e.getEntity() instanceof Player)return;
		for (Scenarios scenarios : Scenarios.values()) {
			for (String listenNameSC : scenarios.iScenario.getListener()) {
				if (e.getEventName().equals(listenNameSC)) {
					scenarios.iScenario.handle(e.getEventName(), e);
					Console.sendMessageDebug("�eScenario �f"+scenarios.iScenario.getName()+" �ahandle �f"+e.getEventName()+" �aobject �e"+e);
				}
			}
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void PlayerDeathEvent_(PlayerDeathEvent e) {
		for (Scenarios scenarios : Scenarios.values()) {
			for (String listenNameSC : scenarios.iScenario.getListener()) {
				if (e.getEventName().equals(listenNameSC)) {
					scenarios.iScenario.handle(e.getEventName(), e);
					Console.sendMessageDebug("�eScenario �f"+scenarios.iScenario.getName()+" �ahandle �f"+e.getEventName()+" �aobject �e"+e);
				}
			}
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void BlockBreakEvent_(BlockBreakEvent e) {
		for (Scenarios scenarios : Scenarios.values()) {
			for (String listenNameSC : scenarios.iScenario.getListener()) {
				if (e.getEventName().equals(listenNameSC)) {
					scenarios.iScenario.handle(e.getEventName(), e);
					Console.sendMessageDebug("�eScenario �f"+scenarios.iScenario.getName()+" �ahandle �f"+e.getEventName()+" �aobject �e"+e);
				}
			}
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void EntityDamageByEntityEvent_(EntityDamageByEntityEvent e) {
		for (Scenarios scenarios : Scenarios.values()) {
			for (String listenNameSC : scenarios.iScenario.getListener()) {
				if (e.getEventName().equals(listenNameSC)) {
					scenarios.iScenario.handle(e.getEventName(), e);
					Console.sendMessageDebug("�eScenario �f"+scenarios.iScenario.getName()+" �ahandle �f"+e.getEventName()+" �aobject �e"+e);
				}
			}
		}
	}
}