package mc.bloostrynetwork.antox11200.manager.game.scenarios.depends;

import mc.bloostrynetwork.antox11200.manager.game.scenarios.BareBones;
import mc.bloostrynetwork.antox11200.manager.game.scenarios.BowSwap;
import mc.bloostrynetwork.antox11200.manager.game.scenarios.CutClean;
import mc.bloostrynetwork.antox11200.manager.game.scenarios.DoubleOres;
import mc.bloostrynetwork.antox11200.manager.game.scenarios.TimeBomb;
import mc.bloostrynetwork.antox11200.manager.game.scenarios.TripleOres;
import mc.bloostrynetwork.antox11200.manager.game.scenarios.WebCage;

public enum Scenarios {

	CUTCLEAN("CutClean", new CutClean()),
	TIMEBOMB("TimeBomb", new TimeBomb()),
	WEBCAGE("WebCage", new WebCage()),
	BOWSWAP("BowSwap", new BowSwap()),
	DOUBLEORES("DoubleOres", new DoubleOres()),
	TRIPLEORES("TripleOres", new TripleOres()),
	BAREBONES("BareBones", new BareBones());

	public String name;
	public IScenario iScenario;

	private Scenarios(String name, IScenario iScenario) {
		this.name = name;
		this.iScenario = iScenario;
	}
}
