package mc.bloostrynetwork.antox11200.manager.game.scenarios;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import mc.bloostrynetwork.antox11200.BloostryUHC;
import mc.bloostrynetwork.antox11200.manager.game.scenarios.depends.IScenario;

public class TimeBomb implements IScenario {

	public static final String PREFIX = "�8[�cTimeBomb�8] ";

	public boolean enabled = false;

	@Override
	public String getName() {
		return "TimeBomb";
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}

	@Override
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	@Override
	public String getDescription() {
		return "�6Lorsqu'un joueur meurt son stuff est mis,�6 dans un double coffre qui explosera au bout de 30 secondes.";
	}

	@Override
	public ItemStack getIcon() {
		return new ItemStack(Material.TNT);
	}

	@Override
	public ArrayList<String> getListener() {
		ArrayList<String> list = new ArrayList<>();
		list.add("PlayerDeathEvent");
		list.add("BlockBreakEvent");
		return list;
	}

	public List<Block> timebomb = new ArrayList<>();

	@Override
	public void handle(String eventName, Object eventObject) {
		if (!isEnabled())return;
		if (eventName.equals("PlayerDeathEvent")) {
			PlayerDeathEvent e = (PlayerDeathEvent) eventObject;
			final Player p = e.getEntity().getPlayer();

			double plocx = p.getLocation().getBlockX();
			double plocx1 = p.getLocation().getBlockX() - 1;
			double plocy = p.getLocation().getBlockY();
			double plocz = p.getLocation().getBlockZ();

			final Location chest1 = new Location(Bukkit.getWorld(p.getWorld().getName()), plocx, plocy, plocz);
			final Location chest2 = new Location(Bukkit.getWorld(p.getWorld().getName()), plocx1, plocy, plocz);

			chest1.getBlock().setType(Material.CHEST);
			chest2.getBlock().setType(Material.CHEST);

			timebomb.add(chest1.getBlock());
			timebomb.add(chest2.getBlock());

			Chest c = (Chest) chest1.getBlock().getState();
			Chest c2 = (Chest) chest2.getBlock().getState();
			final Inventory inv = c.getInventory();
			final Inventory inv2 = c2.getInventory();

			e.setKeepInventory(true);

			for (ItemStack item : p.getInventory().getContents()) {
				if (item != null) {
					if (inv.getContents().length < inv.getSize()) {
						inv.addItem(item);
					} else {
						inv2.addItem(item);
					}
				}
			}

			for (ItemStack item : p.getInventory().getArmorContents()) {
				if (item != null) {
					if (inv.getContents().length < inv.getSize()) {
						inv.addItem(item);
					} else {
						inv2.addItem(item);
					}
				}
			}

			p.getInventory().clear();
			p.getInventory().setArmorContents(null);

			ItemStack goldenhead = new ItemStack(Material.GOLDEN_APPLE);
			ItemMeta goldenheadm = goldenhead.getItemMeta();
			goldenheadm.setDisplayName("�6Golden Head");
			goldenhead.setItemMeta(goldenheadm);

			inv.addItem(goldenhead);

			Bukkit.getScheduler().scheduleSyncDelayedTask(BloostryUHC.getInstance(), new Runnable() {
				public void run() {
					inv.clear();
					chest1.getBlock().setType(Material.AIR);
					chest2.getBlock().setType(Material.AIR);
					Bukkit.broadcastMessage(PREFIX + "�cLe coffre de�e " + p.getName() + " �ca explos� !");
					Bukkit.getWorld(p.getWorld().getName()).createExplosion(chest1, 6);
					Bukkit.getWorld(p.getWorld().getName()).createExplosion(chest2, 6);
					timebomb.remove(chest1.getBlock());
					timebomb.remove(chest2.getBlock());
				}
			}, 20 * 30);
		} else if (eventName.equals("BlockBreakEvent")) {
			BlockBreakEvent e = (BlockBreakEvent) eventObject;

			Block b = e.getBlock();
			Player p = e.getPlayer();
			
			if(timebomb.contains(b)){
				e.setCancelled(true);
				p.sendMessage(PREFIX+"Vous ne pouvez pas casser ce coffre !");
			}
		}
	}
}
