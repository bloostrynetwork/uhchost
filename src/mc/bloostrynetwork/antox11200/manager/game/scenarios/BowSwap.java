package mc.bloostrynetwork.antox11200.manager.game.scenarios;

import java.util.ArrayList;
import java.util.Random;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;

import mc.bloostrynetwork.antox11200.manager.game.scenarios.depends.IScenario;

public class BowSwap implements IScenario {

	private boolean enabled = false;

	@Override
	public String getName() {
		return "BowSwap";
	}

	@Override
	public boolean isEnabled() {
		return this.enabled;
	}

	@Override
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	@Override
	public ItemStack getIcon() {
		return new ItemStack (Material.BOW);
	}

	@Override
	public String getDescription() {
		return "�6Vous avez 45% de chance d'�tre inverser avec l'�nnemi, �6si vous lui tirez dessu.";//',' retour � la ligne
	}

	@Override
	public ArrayList<String> getListener() {
		ArrayList<String> listListener = new ArrayList<>();
		listListener.add("EntityDamageByEntityEvent");
		return listListener;
	}

	@Override
	public void handle(String eventName, Object eventObject) {
		if(!(isEnabled()))return;
		if (eventName.equals("EntityDamageByEntityEvent")) {
			EntityDamageByEntityEvent e = (EntityDamageByEntityEvent) eventObject;
			if (e.getDamager() instanceof Arrow && e.getEntity() instanceof Player
					&& e.getCause().equals(EntityDamageByEntityEvent.DamageCause.PROJECTILE)
					&& ((Arrow) e.getDamager()).getShooter() instanceof Player) {
				Random rand = new Random();
				int i = rand.nextInt(100);

				if (i < 45) {

					Player p = ((Player) ((Arrow) e.getDamager()).getShooter());

					Location senderPos = p.getLocation();

					p.teleport(e.getEntity());
					e.getEntity().teleport(senderPos);
					((Player) e.getEntity()).playSound(p.getLocation(), Sound.ENDERMAN_TELEPORT,  5, 5);
					p.playSound(p.getLocation(), Sound.ENDERMAN_TELEPORT,  5, 5);
				}
			}
		}
	}
}
