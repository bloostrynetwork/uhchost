package mc.bloostrynetwork.antox11200.manager.game.scenarios;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import mc.bloostrynetwork.antox11200.BloostryUHC;
import mc.bloostrynetwork.antox11200.manager.game.scenarios.depends.IScenario;

public class WebCage implements IScenario {

	public static final String PREFIX = "�8[�fWebCage�8] ";

	public boolean enabled = false;

	@Override
	public String getName() {
		return "WebCage";
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}

	@Override
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	@Override
	public String getDescription() {
		return "�6Lorsque que vous tuez un joueur une cage en toile,�6d'araign�e appara�t autour de vous.";
	}

	@Override
	public ItemStack getIcon() {
		return new ItemStack(Material.WEB);
	}

	@Override
	public ArrayList<String> getListener() {
		ArrayList<String> list = new ArrayList<>();
		list.add("PlayerDeathEvent");
		return list;
	}

	public List<Block> timebomb = new ArrayList<>();

	@Override
	public void handle(String eventName, Object eventObject) {
		if (!isEnabled())return;
		if (eventName.equals("PlayerDeathEvent")) {
			PlayerDeathEvent event = (PlayerDeathEvent) eventObject;
			
			Player player = event.getEntity().getPlayer();
			Location loc = player.getLocation();
			
			int bX = (int) loc.getX();
			int bY = (int) loc.getY();
			int bZ = (int) loc.getZ();

			for (int y = bY - 10; y <= bY + 10; y++) {
				for (int x = bX - 10; x <= bX + 10; x++) {
					for (int z = bZ - 10; z <= bZ + 10; z++) {
						Block block = loc.getWorld().getBlockAt(x, y, z);

						int distance = (int) block.getLocation().distance(new Location(loc.getWorld(), bX, bY, bZ));

						if (distance == 4 && block.getType() == Material.AIR) {
							block.setType(Material.WEB);
							block.getState().update(true);
						}
					}
				}
			}
		}
	}
}
