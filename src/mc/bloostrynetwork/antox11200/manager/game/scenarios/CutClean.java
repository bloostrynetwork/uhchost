package mc.bloostrynetwork.antox11200.manager.game.scenarios;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Ageable;
import org.bukkit.entity.Chicken;
import org.bukkit.entity.Cow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.ExperienceOrb;
import org.bukkit.entity.Pig;
import org.bukkit.entity.Player;
import org.bukkit.entity.Rabbit;
import org.bukkit.entity.Sheep;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;

import mc.bloostrynetwork.antox11200.manager.game.scenarios.depends.IScenario;
import mc.bloostrynetwork.antox11200.manager.game.scenarios.depends.Scenarios;

public class CutClean implements IScenario {

	private boolean enabled = false;

	@Override
	public String getName() {
		return "CutClean";
	}

	@Override
	public boolean isEnabled() {
		return this.enabled;
	}

	@Override
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	@Override
	public ItemStack getIcon() {
		return new ItemStack(Material.COOKED_BEEF);
	}

	@Override
	public String getDescription() {
		return "�6Les min�rais et la nourriture sont automatiquement cuites";
	}

	@Override
	public ArrayList<String> getListener() {
		ArrayList<String> listListener = new ArrayList<>();
		listListener.add("BlockBreakEvent");
		listListener.add("EntityDeathEvent");
		return listListener;
	}

	@Override
	public void handle(String eventName, Object eventObject) {
		if (eventName.equals("BlockBreakEvent")) {
			BlockBreakEvent e = (BlockBreakEvent) eventObject;
			if (isEnabled() && !Scenarios.BAREBONES.iScenario.isEnabled() && !Scenarios.DOUBLEORES.iScenario.isEnabled()&&!Scenarios.TRIPLEORES.iScenario.isEnabled()) {
				Block block = e.getBlock();
				Player p = e.getPlayer();
				if (block.getType() == Material.IRON_ORE) {
					e.setCancelled(true);
					block.getLocation().getBlock().setType(Material.AIR);
					p.getWorld().dropItemNaturally(p.getLocation(), new ItemStack(Material.IRON_INGOT));
					((ExperienceOrb) block.getWorld().spawn(block.getLocation(), ExperienceOrb.class)).setExperience(1);
					block.getState().update();
				} else if (block.getType() == Material.GOLD_ORE) {
					e.setCancelled(true);
					block.getLocation().getBlock().setType(Material.AIR);
					p.getWorld().dropItemNaturally(p.getLocation(), new ItemStack(Material.GOLD_INGOT));
					((ExperienceOrb) block.getWorld().spawn(block.getLocation(), ExperienceOrb.class)).setExperience(2);
					block.getState().update();
				}
			}
		} 
		if (eventName.equals("EntityDeathEvent")) {
			EntityDeathEvent event = (EntityDeathEvent) eventObject;
			if (isEnabled()) {
				final List<ItemStack> drops = event.getDrops();
				final Entity entity = event.getEntity();
				if (entity instanceof Ageable && !((Ageable) entity).isAdult()) {
					drops.clear();
					return;
				}

				if (entity instanceof Cow) {
					drops.clear();
					drops.add(new ItemStack(Material.COOKED_BEEF, 3));
					drops.add(new ItemStack(Material.LEATHER));
					event.setDroppedExp(1);
					return;
				}

				if (entity instanceof Chicken) {
					drops.clear();
					drops.add(new ItemStack(Material.COOKED_CHICKEN, 3));
					drops.add(new ItemStack(Material.FEATHER, 2));
					event.setDroppedExp(1);
					return;
				}

				if (entity instanceof Pig) {
					drops.clear();
					drops.add(new ItemStack(Material.GRILLED_PORK, 3));
					event.setDroppedExp(1);
					return;
				}

				if (entity instanceof Sheep) {
					final Sheep sheep = (Sheep) entity;

					drops.clear();
					drops.add(new ItemStack(Material.WOOL, 1, sheep.getColor().getWoolData()));
					drops.add(new ItemStack(Material.COOKED_MUTTON, 2));
					event.setDroppedExp(1);
					return;
				}

				if (entity instanceof Rabbit) {
					drops.clear();
					drops.add(new ItemStack(Material.COOKED_RABBIT, 2));
					drops.add(new ItemStack(Material.RABBIT_HIDE));
					event.setDroppedExp(1);
				}
			}
		}
	}
}
