package mc.bloostrynetwork.antox11200.manager.game.scenarios;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import mc.bloostrynetwork.antox11200.manager.game.scenarios.depends.IScenario;

public class BareBones implements IScenario {

	private boolean enabled = false;

	@Override
	public String getName() {
		return "BareBones";
	}

	@Override
	public boolean isEnabled() {
		return this.enabled;
	}

	@Override
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	@Override
	public ItemStack getIcon() {
		return new ItemStack(Material.ARROW);
	}

	@Override
	public String getDescription() {
		return "�6Les min�rais de diamant et d'or se transforme en l'ingot de fer";
	}

	@Override
	public ArrayList<String> getListener() {
		ArrayList<String> listListener = new ArrayList<>();
		listListener.add("BlockBreakEvent");
		listListener.add("EntityDeathEvent");
		return listListener;
	}

	@Override
	public void handle(String eventName, Object eventObject) {
		if (!isEnabled())return;
		if(eventName.equals("BlockBreakEvent")) {
			BlockBreakEvent e = (BlockBreakEvent) eventObject;

			Block b = e.getBlock();
			Material bt = b.getType();

			if (bt == Material.IRON_ORE || bt == Material.DIAMOND_ORE || bt == Material.GOLD_ORE) {
				e.setCancelled(true);
				b.getLocation().getBlock().setType(Material.AIR);
				Bukkit.getWorld(b.getWorld().getName()).dropItem(b.getLocation(), new ItemStack(Material.IRON_INGOT));
			}
		} else if(eventName.equals("EntityDeathEvent")) {
			EntityDeathEvent e = (EntityDeathEvent) eventObject;

			if(e.getEntity() instanceof Player) {
				Player player = (Player) e.getEntity();
				player.getLocation().getWorld().dropItem(player.getLocation(), new ItemStack(Material.DIAMOND));
				player.getLocation().getWorld().dropItem(player.getLocation(), new ItemStack(Material.ARROW, 16));

				ItemStack goldenhead = new ItemStack(Material.GOLDEN_APPLE);
				ItemMeta goldenheadm = goldenhead.getItemMeta();
				goldenheadm.setDisplayName("�6Golden Head");
				goldenhead.setItemMeta(goldenheadm);
				
				player.getLocation().getWorld().dropItem(player.getLocation(), new ItemStack(goldenhead));
			}
		}
	}
}
