package mc.bloostrynetwork.antox11200.manager.game.rules;

import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.LeavesDecayEvent;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerPortalEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import mc.bloostrynetwork.antox11200.manager.game.GameManager;

public class RulesListeners implements Listener {

	@EventHandler
	public void breakItem(BlockBreakEvent e) {
		if(e.isCancelled())return;
		if (e.getBlock().getType() == Material.GRAVEL) {
			if (new Random().nextInt(100) <= RulesManager.FLINT_DROP_RATE) {
				e.setCancelled(true);
				Bukkit.getWorld(e.getBlock().getLocation().getWorld().getName()).dropItem(e.getBlock().getLocation(),
						new ItemStack(Material.FLINT));
				e.getBlock().breakNaturally();
			}
		} else if (e.getBlock().getType() == Material.LEAVES || e.getBlock().getType() == Material.LEAVES_2) {
			if (new Random().nextInt(100) <= RulesManager.APPLE_DROP_RATE) {
				e.setCancelled(true);
				Bukkit.getWorld(e.getBlock().getLocation().getWorld().getName()).dropItem(e.getBlock().getLocation(),
						new ItemStack(Material.APPLE));
				e.getBlock().breakNaturally();
			}
		}
	}

	@EventHandler
	public void blockDispawn(LeavesDecayEvent e) {
		if (new Random().nextInt(100) <= RulesManager.APPLE_DROP_RATE) {
			Bukkit.getWorld(e.getBlock().getLocation().getWorld().getName()).dropItem(e.getBlock().getLocation(),
					new ItemStack(Material.APPLE));
		}
	}

	@EventHandler
	public void PlayerPortalEvent_(PlayerPortalEvent e) {
		if(e.isCancelled())return;
		if (RulesManager.NETHER)
			return;
		World world = e.getTo().getWorld();
		if (world.getName().contains("nether")) {
			e.setCancelled(true);
			e.getPlayer().sendMessage(GameManager.PREFIX + "�cVous ne pouvez pas acc�der au nether !");
		}if (world.getName().contains("the_end")) {
			e.setCancelled(true);
			e.getPlayer().sendMessage(GameManager.PREFIX + "�cVous ne pouvez pas acc�der � l'end !");
		}
	}

	@EventHandler
	public void horse(EntitySpawnEvent e) {
		if(e.isCancelled())return;
		if (!(RulesManager.HORSE))
			return;
		if (e.getEntity() instanceof Horse) {
			e.setCancelled(true);
		}
	}

	@EventHandler
	public void goldenhead(PlayerItemConsumeEvent e) {
		if(e.isCancelled())return;
		if (!RulesManager.GOLDENHEAD)
			return;
		if (e.getItem().getType() == Material.GOLDEN_APPLE) {
			if (e.getItem().hasItemMeta()) {
				if (e.getItem().getItemMeta().getDisplayName().equals("�6Golden Head")) {
					Player player = e.getPlayer();
					new PotionEffect(PotionEffectType.ABSORPTION, 2400, 0).apply(player);
					new PotionEffect(PotionEffectType.REGENERATION, 160, 1).apply(player);
				}
			}
		}
	}
}
