package mc.bloostrynetwork.antox11200.manager.game;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ThreadLocalRandom;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import mc.bloostrynetwork.antox11200.BloostryUHC;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.DateUtils;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.Hologram;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.console.Console;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.player.BloostryPlayer;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.server.ServerStats;
import mc.bloostrynetwork.antox11200.inventorys.GuiMain;
import mc.bloostrynetwork.antox11200.items.Item;
import mc.bloostrynetwork.antox11200.manager.game.rules.RulesManager;
import mc.bloostrynetwork.antox11200.manager.game.runnable.BorderRunnable;
import mc.bloostrynetwork.antox11200.manager.game.runnable.DamageRunnable;
import mc.bloostrynetwork.antox11200.manager.game.runnable.PvPRunnable;
import mc.bloostrynetwork.antox11200.manager.game.scenarios.depends.Scenarios;
import mc.bloostrynetwork.antox11200.manager.game.team.Team;
import mc.bloostrynetwork.antox11200.manager.game.team.TeamType;
import mc.bloostrynetwork.antox11200.manager.game.worldedit.LoadSchematics;
import mc.bloostrynetwork.antox11200.player.UPlayer;

public class GameManager {

	public static final String NAME = "UHCHost";

	public static final String PREFIX = "�6[�eBloostryUHC�6] �f";
	public static final String PREFIX_TEAM = "�8[�aTeam�8] �a";

	public static final int VAR_MAX_PLAYERS = 80;

	public static String HOST_NAME = null;

	public static UPlayer WINNER = null;

	public static int HOST_ID = 0;

	public static int PVP_MIN_TIME = 10 * 60;
	public static int PVP_MAX_TIME = 20 * 60;

	public static int BORDER_MIN_TIME = 10 * 60;
	public static int BORDER_MAX_TIME = 30 * 60;

	public static BorderRunnable BORDER_RUNNABLE;
	public static PvPRunnable PVP_RUNNABLE;
	public static DamageRunnable DOMAGE_RUNNABLE;

	public static GameState gameState = GameState.HOST;

	public static int MAX_PLAYERS = 80;

	public static int BORDER_SIZE = 2000;
	public static int BORDER_MAX_SIZE = 2000;
	public static int BORDER_MIN_SIZE = 500;

	public static HashMap<String, UPlayer> players = new HashMap<>();

	public static List<String> playersInGame = new ArrayList<>();

	public static TeamType teamType = TeamType.FFA;

	public static boolean PVP = false;
	public static boolean DAMAGE = false;
	public static boolean FINISH_GENERATE = false;

	private static World gameWorld;
	private static World lobbyWorld;

	public static void init() {
		resetWorlds("uhc");
		resetWorlds("uhc_lobby");

		lobbyWorld = new WorldCreator("uhc_lobby").createWorld();

		Bukkit.broadcastMessage(PREFIX + "�6Cr�ation du monde ...");

		WorldCreator wc = new WorldCreator("uhc");
		wc.seed(lobbyWorld.getSeed());
		gameWorld = wc.createWorld();

		Bukkit.broadcastMessage(PREFIX + "�6Cr�ation du monde termin�e.");
		Bukkit.broadcastMessage(PREFIX + "�6Lancement de la g�n�ration du monde ...");

		loadSchematic();

		BORDER_RUNNABLE = new BorderRunnable(BORDER_MAX_TIME);
		PVP_RUNNABLE = new PvPRunnable(PVP_MAX_TIME);
	}

	public static void loadSchematic() {
		for (Entity entities : lobbyWorld.getEntities()) {
			if (!(entities instanceof Player)) {
				entities.remove();
				Console.sendMessageDebug(entities.getName() + " �cwas killed by generation schematic !");
			}
		}
		Console.sendMessageDebug("Start loading schematics ...");
		LoadSchematics.loadSchematic(lobbyWorld, "Lobby");
		Console.sendMessageDebug("Finish loading schematics ...");
		return;
	}

	public static boolean hasTeam() {
		return teamType != TeamType.FFA;
	}

	public static Location getSpawnLocation() {
		return new Location(Bukkit.getWorld("uhc"), 0.5, 178.5, 0.5);
	}

	public static Location getLobbyLocation() {
		return new Location(Bukkit.getWorld("uhc_lobby"), 0.5, 178.5, 0.5);
	}

	public static void open(String arg2) {
		if (arg2.equalsIgnoreCase("pre-whitelist")) {
			gameState = GameState.PRE_WHITELIST;
			Bukkit.broadcastMessage(PREFIX + "�6Le serveur est ouvert pour les pr�-whitelist.");
		} else if (arg2.equalsIgnoreCase("all")) {
			gameState = GameState.GLOBAL_WAITING;
			Bukkit.broadcastMessage(PREFIX + "�6Le serveur est ouvert pour tous les joueurs.");
		}
	}

	// #####################################################

	public static void restart() {
		for (Player players : Bukkit.getOnlinePlayers()) {
			players.kickPlayer("�cD�connexion du serveur \n�7Red�marrage du serveur et r�initialisation des mondes.");
		}
		stop();
		Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "restart");
	}

	public static void stopGame(String human, String reason) {
		if (gameState == GameState.FINISH) {
			for (Player players : Bukkit.getOnlinePlayers()) {
				players.kickPlayer("�cD�connexion du serveur \n�7La partie est termin�e.");
			}
			int number = (int) getHostNumber();
			BloostryUHC.getInstance().getConfig().set("host-number", number + 1);
			BloostryUHC.getInstance().saveConfig();
		} else {
			for (Player players : Bukkit.getOnlinePlayers()) {
				players.kickPlayer("�cD�connexion du serveur \n�7La partie � �tait annul�e par �e" + human
						+ "�7.\n�7raison:�c" + reason + "�.");
			}
		}
		restart();
	}

	public static int getHostNumber() {
		return (int) BloostryUHC.getInstance().getConfig().get("host-number");
	}

	public static void stop() {
		BloostryUHC.getInstance().saveDefaultConfig();
		for (Player players : Bukkit.getOnlinePlayers()) {
			players.kickPlayer("�cD�connexion du serveur \n�7R�initialisation des mondes.");
		}
		resetWorlds(getGameWorld().getName());
	}

	public static void resetWorlds(String world) {
		gameState = GameState.RESET;
		Bukkit.getServer().unloadWorld(world, false);
		File world_file = new File(world);
		if (world_file.exists()) {
			deleteFiles(world_file);
		}
		world_file.delete();

		/*
		 * Bukkit.getServer().unloadWorld(world + "_nether", true); File
		 * world_nether_file = new File(world + "_nether"); if
		 * (world_nether_file.exists()) { deleteFiles(world_nether_file); }
		 * world_nether_file.delete();
		 * 
		 */
	}

	private static void deleteFiles(File file) {
		for (File files : file.listFiles()) {
			if (files.isFile()) {
				files.delete();
				Bukkit.broadcastMessage(PREFIX + "�cThe file �e'" + files.getAbsolutePath() + "' �cis delete !");
			} else if (files.isDirectory()) {
				deleteFiles(new File(files.getAbsolutePath()));
			}
		}
	}

	private static ArrayList<Location> blocksLocation = new ArrayList<>();
	private static ArrayList<Player> teleportationPlayers = new ArrayList<>();
	private static HashMap<String, Location> teleportationsLocation = new HashMap<>();

	private static int task;

	@SuppressWarnings("deprecation")
	public static void start(String hoster) {
		HOST_NAME = hoster;
		
		players.get(hoster).specMode = true;

		Item golden_head = new Item(Material.GOLDEN_APPLE, "�6Golden Head", null, null);
		ShapedRecipe goldenHead = new ShapedRecipe(golden_head.toItemStack());

		goldenHead.shape("ggg", "ghg", "ggg");
		goldenHead.setIngredient('g', Material.GOLD_INGOT);
		goldenHead.setIngredient('h', Material.SKULL_ITEM, 3);

		Bukkit.getServer().addRecipe(goldenHead);

		gameState = GameState.TELEPORTING;

		Bukkit.broadcastMessage(PREFIX + "�cVeuillez ne pas vous d�connectez pendant la t�l�portation !");

		for (String playersName : players.keySet()) {
			UPlayer uplayer = players.get(playersName);
			if (!uplayer.specMode) {
				if (!uplayer.connected) {
					players.remove(playersName);
				} else {
					teleportationPlayers.add(uplayer.getPlayer());
					teleportationsLocation.put(uplayer.getName(), getPlayerLocation());
				}
			} else {
				uplayer.joinInStaff();
			}
		}

		task = Bukkit.getScheduler().scheduleSyncRepeatingTask(BloostryUHC.getInstance(), new BukkitRunnable() {
			int playerPos = 0;

			// ChunkProviderServer cps = ((CraftWorld)
			// getGameWorld()).getHandle().chunkProviderServer;

			@Override
			public void run() {
				Player player = teleportationPlayers.get(playerPos);

				if (Bukkit.getPlayer(player.getName()) == null) {
					players.remove(player.getName());
					return;
				}

				Location playerRandomLocation = teleportationsLocation.get(player.getName());

				player.setWalkSpeed(0.0F);
				player.getInventory().addItem(new ItemStack(Material.COOKED_BEEF, 25));

				UPlayer uplayer = GameManager.players.get(player.getName());

				playerPos++;

				for (int x = -2; x < 2; x++) {
					for (int z = -2; z < 2; z++) {
						Location location = new Location(playerRandomLocation.getWorld(),
								playerRandomLocation.getX() + x, playerRandomLocation.getY() - 6,
								playerRandomLocation.getZ() + z);
						location.getBlock().setType(Material.GLASS);
						blocksLocation.add(location);
					}
				}

				Console.sendMessageDebug("Creation the platform");

				player.teleport(playerRandomLocation);

				/*
				 * Bukkit.broadcastMessage(PREFIX + "�6T�l�poration de �e" + uplayer.getName() +
				 * " �7[�e" + playerPos . + "�8/�6" + Bukkit.getOnlinePlayers().size() + "�7]");
				 */

				playersInGame.add(player.getName());

				if ((playerPos) >= players.size()) {
					Bukkit.broadcastMessage("�7�M--------------------------------");
					Bukkit.broadcastMessage("");
					Bukkit.broadcastMessage("�6Host: �e" + hoster);
					Bukkit.broadcastMessage("");
					Bukkit.broadcastMessage("�6Sc�narios:");
					for (Scenarios scenarios : Scenarios.values()) {
						if (scenarios.iScenario.isEnabled()) {
							Bukkit.broadcastMessage("  �8- �e" + scenarios.iScenario.getName());
						}
					}
					Bukkit.broadcastMessage("");
					Bukkit.broadcastMessage("�6R�gles:");
					Bukkit.broadcastMessage(
							"  �6Golden Head: " + (RulesManager.GOLDENHEAD ? "�aActiv�es" : "�cD�sactiv�es"));
					Bukkit.broadcastMessage("  �6Cheveaux: " + (RulesManager.HORSE ? "�aActiv�s" : "�cD�sactiv�s"));
					Bukkit.broadcastMessage("  �6Nether: " + (RulesManager.NETHER ? "�aActiv�" : "�cD�sactiv�"));
					Bukkit.broadcastMessage("");
					Bukkit.broadcastMessage("  �6Taille de bordure: �e" + BORDER_SIZE + "�8x�e" + BORDER_SIZE
							+ " �7(�c-1000�8/�a+1000�7)");
					Bukkit.broadcastMessage(
							"  �6Taut de drop pour les flint: �e" + RulesManager.FLINT_DROP_RATE + "�6%");
					Bukkit.broadcastMessage(
							"  �6Taut de drop pour les pommes: �e" + RulesManager.APPLE_DROP_RATE + "�6%");
					Bukkit.broadcastMessage("");
					Bukkit.broadcastMessage("�6Temps:");

					DOMAGE_RUNNABLE = new DamageRunnable(2 * 60);

					PVP_RUNNABLE.start();

					BORDER_RUNNABLE.getWorldBorder().setSize(BORDER_SIZE);

					BORDER_RUNNABLE.start();
					DOMAGE_RUNNABLE.start();

					Bukkit.broadcastMessage("  �6Dommage: �e" + DateUtils.getTimerDate(DOMAGE_RUNNABLE.getTimer()));
					Bukkit.broadcastMessage("  �6Bordure: �e" + DateUtils.getTimerDate(BORDER_RUNNABLE.getTimer()));
					Bukkit.broadcastMessage("  �6PvP: �e" + DateUtils.getTimerDate(PVP_RUNNABLE.getTimer()));
					Bukkit.broadcastMessage("");
					Bukkit.broadcastMessage("�7�M--------------------------------");

					getGameWorld().setGameRuleValue("naturalRegeneration", "false");
					getGameWorld().setTime(0L);

					for (Scenarios scenarios : Scenarios.values()) {
						for (String players : players.keySet()) {
							scenarios.iScenario.handle("items", players);
						}
					}

					gameState = GameState.FARMING;

					for (Location locations : blocksLocation) {
						locations.getBlock().setType(Material.AIR);
					}

					for (Player players : Bukkit.getOnlinePlayers()) {
						players.setWalkSpeed(0.2F);
					}

					Bukkit.broadcastMessage(PREFIX + "�6La partie commence, bon jeux");
					Bukkit.getScheduler().cancelTask(task);
				}
			}
		}, 0, 20);
	}

	private static int quarterMap = 4;

	private static Location getPlayerLocation() {
		Location location = null;

		int x = 0;
		int z = 0;

		// Random rand = new Random();

		if (quarterMap == 4) {
			x = -ThreadLocalRandom.current().nextInt(BORDER_SIZE / 2);
			z = ThreadLocalRandom.current().nextInt(BORDER_SIZE / 2);
			quarterMap = 1;
		}
		if (quarterMap == 3) {
			x = ThreadLocalRandom.current().nextInt(BORDER_SIZE / 2);
			z = -ThreadLocalRandom.current().nextInt(BORDER_SIZE / 2);
			quarterMap = 4;
		}
		if (quarterMap == 2) {
			x = -ThreadLocalRandom.current().nextInt(BORDER_SIZE / 2);
			z = -ThreadLocalRandom.current().nextInt(BORDER_SIZE / 2);
			quarterMap = 3;
		}
		if (quarterMap == 1) {
			x = ThreadLocalRandom.current().nextInt(BORDER_SIZE / 2);
			z = ThreadLocalRandom.current().nextInt(BORDER_SIZE / 2);
			quarterMap = 2;
		}

		location = new Location(getGameWorld(), x, 160, z);

		return location;
	}

	public static void loadChunksOnRadius(World world, int X, int Z, int radius) {
		Location to = new Location(world, X, 120, Z);

		Long latence = null;

		for (int x = -radius; x < radius; x++) {
			for (int z = -radius; z < radius; z++) {
				int chunkX = (to.getBlockX() / 16 + x);
				int chunkZ = (to.getBlockZ() / 16 + z);
				if (to.getWorld().getChunkAt(chunkX, chunkZ).load(true)) {
					if (latence != null) {
						latence = System.currentTimeMillis() - latence;
					} else {
						latence = 0L;
					}
					Console.sendMessageDebug("�6Le chunk �8(�e" + chunkX + "�7,�e" + chunkZ
							+ "�8) �6est charg�e ! �8(�eLatence: �f" + latence + " ms�8)");
					latence = System.currentTimeMillis();
				}
			}
		}
	}

	private static ArrayList<Integer> chunksX = new ArrayList<>();
	private static ArrayList<Integer> chunksZ = new ArrayList<>();

	private static BukkitTask chunkLoaderTask;

	public static boolean loadChunks(String world, int radius, int sx, int sz) {
		Bukkit.broadcastMessage(
				PREFIX + "�6G�n�ration de �e" + world + " �6depuis �8(�6x: �e" + sx + "�6, �6z: �e" + sz + "�8)");

		chunkLoaderTask = Bukkit.getScheduler().runTaskTimer(BloostryUHC.getInstance(), new Runnable() {

			int maxX = +(radius / 2);
			int maxZ = +(radius / 2);
			int minX = -(radius / 2);
			int minZ = -(radius / 2);

			int maxChunkX = maxX / 16;
			int maxChunkZ = maxZ / 16;

			int minChunkX = minX / 16;
			int minChunkZ = minZ / 16;

			int chunks = radius * radius / (16 * 16);

			Long currentTime = System.currentTimeMillis();

			int chunkSucces = 0;
			int chunkFailed = 0;

			World w = Bukkit.getWorld(world);

			Integer chunkX = minChunkX;
			Integer chunkZ = minChunkZ;

			Long chunkLoadPerSecondLatence = 0L;

			int chunkLoadOnSecond = 0;

			int chunkLoadPerSecond = 0;

			@Override
			public void run() {
				if (chunkLoadPerSecondLatence == 0L)
					chunkLoadPerSecondLatence = System.currentTimeMillis();
				if (chunkX <= maxChunkX) {
					chunkX++;
				} else {
					if (chunkZ <= maxChunkZ) {
						chunkZ++;
						chunkX = minChunkX;
					} else {
						Bukkit.broadcastMessage("�7�m----------------------------------");
						Bukkit.broadcastMessage("");
						Bukkit.broadcastMessage("�7R�sultat du chargement:");
						Bukkit.broadcastMessage("  �6Chunk charg�: �a" + chunkSucces + "�7/�6" + chunks);
						Bukkit.broadcastMessage("  �6Chunk mal charg�: �c" + chunkFailed + "�7/�6" + chunks);
						Bukkit.broadcastMessage("  �6Temps �coul�: �e"
								+ DateUtils.getTimerDate((Long) System.currentTimeMillis() - currentTime));
						Bukkit.broadcastMessage("");
						Bukkit.broadcastMessage("�7�m----------------------------------");
						chunkLoaderTask.cancel();
						Console.sendMessageDebug("chunkLoaderTask.cancel();");
						return;
					}
				}

				Chunk chunk = w.getChunkAt(chunkX, chunkZ);
				if (chunk.load(true)) {
					chunkSucces++;
				} else {
					chunkFailed++;
				}

				chunkLoadOnSecond++;

				long ramUsed = ServerStats.getUsedMemoryRAM();
				long ramMax = ServerStats.getMaximumMemoryRAM();

				String tps = ServerStats.getTPS();

				chunksX.add(chunkX);
				chunksZ.add(chunkZ);

				if ((System.currentTimeMillis() - chunkLoadPerSecondLatence) >= 1000) {
					chunkLoadPerSecondLatence = System.currentTimeMillis();
					chunkLoadPerSecond = chunkLoadOnSecond;
					chunkLoadOnSecond = 0;
				}

				Console.sendMessageDebug("�7�m------------------------------------");
				Console.sendMessageDebug("�6");
				Console.sendMessageDebug(
						"�aLe chunk �8(�7x: �e" + chunkX + "�8, �7z: �e" + chunkZ + "�8) �aa bien charg�.");
				Console.sendMessageDebug("");
				Console.sendMessageDebug("�6Stats:");
				Console.sendMessageDebug("  �6M�moire: �e" + ramUsed + "�7/�e" + ramMax + " �6MB");
				Console.sendMessageDebug("  �6Ticks: �e" + tps + "/s �7/ �e20.00/s");
				Console.sendMessageDebug("  �6Chunk charg�: �e" + chunkSucces + "�7/�e" + chunks);
				Console.sendMessageDebug("  �6Nom du monde: �e" + world);
				Console.sendMessageDebug("  �6Temps �coul�: �e"
						+ DateUtils.getTimerDate((Long) System.currentTimeMillis() - currentTime));
				Console.sendMessageDebug("�6");
				Console.sendMessageDebug("�7�m------------------------------------");

				for (UPlayer uplayers : GameManager.players.values()) {
					uplayers.sendActionBar("�6Chunk charg�: �e" + chunkSucces + "�f/�e" + chunks + " �8(�e"
							+ chunkLoadPerSecond + "�6/s�8) �7| �6Temps �coul�: �e"
							+ DateUtils.getTimerDate((Long) System.currentTimeMillis() - currentTime)
							+ " �7| �6M�moire utilis�e: �e" + ramUsed + "�6MB�f/�e" + ramMax + "�6MB");
				}
			}
		}, 0L, 1L);

		/*
		 * Bukkit.getScheduler().runTaskAsynchronously(BloostryUHC.getInstance(), new
		 * Runnable() {
		 * 
		 * @Override public void run() { int maxX = +(radius / 2); int maxZ = +(radius /
		 * 2); int minX = -(radius / 2); int minZ = -(radius / 2);
		 * 
		 * int chunks = radius * radius / (16 * 16);
		 * 
		 * Long currentTime = System.currentTimeMillis();
		 * 
		 * int total = 0;
		 * 
		 * World w = Bukkit.getWorld(world);
		 * 
		 * ChunkProviderServer cps = ((CraftWorld) w).getHandle().chunkProviderServer;
		 * 
		 * for (Integer cx = minX / 16 ; cx <= maxX / 16; cx++) { for (Integer cz = minZ
		 * / 16; cz <= maxZ / 16; cz++) {
		 * 
		 * long ramUsed = BloostryAPIMain.getUsedMemoryRAM(); long ramMax =
		 * BloostryAPIMain.getMaximumMemoryRAM();
		 * 
		 * String tps = BloostryAPIMain.getTPS();
		 * 
		 * net.minecraft.server.v1_8_R3.Chunk chunk =
		 * cps.chunkProvider.getOrCreateChunk(cx, cz);
		 * 
		 * chunkZ.add(cz); chunkX.add(cx);
		 * 
		 * total++;
		 * 
		 * Console.sendMessageDebug("�7�m------------------------------------");
		 * Console.sendMessageDebug("�6"); Console.sendMessageDebug(
		 * "�aLe chunk �8(�7x: �e" + cx + "�8, �7z: �e" + cz + "�8) �aa bien charg�.");
		 * Console.sendMessageDebug(""); Console.sendMessageDebug("�6Stats:");
		 * Console.sendMessageDebug("  �6M�moire: �e" + ramUsed + "�7/�e" + ramMax +
		 * " �6MB"); Console.sendMessageDebug("  �6Ticks: �e" + tps +
		 * "/s �7/ �e20.00/s"); Console.sendMessageDebug("  �6Chunk charg�: �e" + total
		 * + "�7/�e" + chunks); Console.sendMessageDebug("  �6Nom du monde: �e" +
		 * world); Console.sendMessageDebug("  �6Temps �coul�: �e" +
		 * FormatData.getTimerDate((Long) System.currentTimeMillis() - currentTime));
		 * Console.sendMessageDebug("�6");
		 * Console.sendMessageDebug("�7�m------------------------------------");
		 * 
		 * for (UPlayer uplayers : GameManager.players.values()) {
		 * uplayers.sendActionBar( "�6Chunk charg�: �e" + total + "�f/�e" + chunks +
		 * " �7| �6Temps �coul�: �e" + FormatData.getTimerDate((Long)
		 * System.currentTimeMillis() - currentTime) + " �7| �6M�moire utilis�e: �e" +
		 * ramUsed + "�6MB�f/�e" + ramMax + "�6MB"); } } }
		 * 
		 * Bukkit.broadcastMessage("�7�m----------------------------------");
		 * Bukkit.broadcastMessage("");
		 * Bukkit.broadcastMessage("�7R�sultat du chargement:");
		 * Bukkit.broadcastMessage("  �6Chunk charg�: �a" + total + "�7/�6" + chunks);
		 * Bukkit.broadcastMessage("  �6Temps �coul�: �e" +
		 * FormatData.getTimerDate((Long) System.currentTimeMillis() - currentTime));
		 * Bukkit.broadcastMessage("");
		 * Bukkit.broadcastMessage("�7�m----------------------------------");
		 * 
		 * GameManager.gameState = GameState.HOST; } });
		 */
		return false;
	}

	public static void config(Player player) {
		new GuiMain().open(player);
	}

	public static boolean preWhitelist(String arg2, BloostryPlayer iplayer,
			Integer numberWhitelist) {/*
										 * if (arg2.equalsIgnoreCase("add")) {
										 * iplayer.getWhitelist(Bukkit.getServerName()).amount += numberWhitelist;
										 * return true; } else if (arg2.equalsIgnoreCase("remove")) { if
										 * (numberWhitelist > iplayer.getWhitelist(Bukkit.getServerName()).amount) {
										 * iplayer.getWhitelist(Bukkit.getServerName()).amount = 0; } return true; }
										 */
		return false;
	}

	public static Hologram TKHologram = null;

	public static void gameFinish(String winner) {
		WINNER = players.get(winner);

		for (UPlayer uplayers : players.values()) {
			if (uplayers.isOnline()) {
				uplayers.sendTitle(
						"�6Bravo " + (hasTeam()
								? "� l'�quipe de �e" + WINNER.team.getPrefix() + WINNER.team.getOwner().getName()
								: "� �e" + winner) + " �6pour sa victoire",
						"�6Nomdre de kill(s): �E" + WINNER.getKills().size(), 3);
			}
		}

		BORDER_RUNNABLE.cancel();
		PVP_RUNNABLE.cancel();

		Map<String, Integer> topKills = new HashMap<>();

		for (UPlayer uplayers : players.values()) {
			topKills.put(uplayers.getName(), uplayers.getKills().size());
		}

		Location loc = getLobbyLocation();
		for (Player players : Bukkit.getOnlinePlayers()) {
			players.teleport(loc);
			showHologramKill(players);
		}

		gameState = GameState.FINISH;
	}

	// TODO re faire le syst�me de top kill
	public static void showHologramKill(Player player) {
		Map<String, Integer> topKills = new HashMap<>();

		for (UPlayer uplayers : players.values()) {
			topKills.put(uplayers.getName(), uplayers.getKills().size());
		}

		int i = 1;

		List<String> lines = new ArrayList<>();
		lines.add("�8�m-----------------");
		lines.add("�cTop Kills �8(�e10�8)");
		lines.add("�8�m-----------------");
		for (Entry<String, Integer> d : entriesSortedByValues(topKills)) {
			String key = d.getKey();
			int value = d.getValue();
			lines.add("�8[�a" + i + "�8] �e" + key + ": �6" + value);
			i++;
		}
		while (i < 11) {
			lines.add("�8[�a" + i + "�8] �enull: �60");
			i++;
		}
		lines.add("�8�m-----------------");

		Location locHologram = new Location(player.getWorld(), 6.5, 168, 0.5);
		TKHologram = new Hologram(locHologram, lines);
		TKHologram.display(player);
	}

	static <K, V extends Comparable<? super V>> List<Entry<K, V>> entriesSortedByValues(Map<K, V> map) {

		List<Entry<K, V>> sortedEntries = new ArrayList<Entry<K, V>>(map.entrySet());

		Collections.sort(sortedEntries, new Comparator<Entry<K, V>>() {
			@Override
			public int compare(Entry<K, V> e1, Entry<K, V> e2) {
				return e2.getValue().compareTo(e1.getValue());
			}
		});

		return sortedEntries;
	}

	/*
	 * Set<Map.Entry<String,Integer>> set = new TreeSet<Map.Entry<String,
	 * Integer>>(new Comparator<Map.Entry<String, Integer>>() {
	 * 
	 * @Override public int compare(Map.Entry<String, Integer> obj1,
	 * Map.Entry<String, Integer> obj2) { Integer val1 = obj1.getValue(); Integer
	 * val2 = obj2.getValue(); // DUPLICATE VALUE CASE // If the values are equals,
	 * we can't return 0 because the 2 entries would be // considered // as equals
	 * and one of them would be deleted (because we use a set, no // duplicate,
	 * remember!) int compareValues = val1.compareTo(val2); if (compareValues == 0)
	 * { String key1 = obj1.getKey(); String key2 = obj2.getKey(); int compareKeys =
	 * key1.compareTo(key2); if (compareKeys == 0) { // what you return here will
	 * tell us if you keep REAL KEY-VALUE duplicates in // your set // if you want
	 * to, do whatever you want but do not return 0 (but don't break the //
	 * comparator contract!) return 0; } return compareKeys; } return compareValues;
	 * }
	 * 
	 * });
	 * 
	 * set.addAll(aMap.entrySet());
	 * 
	 * Map<String,Integer> myMap = new HashMap<String,Integer>(); for (
	 * Map.Entry<String,Integer> entry : set) {
	 * Console.sendMessageDebug("Added to result map entries: " + entry.getKey() +
	 * " " + entry.getValue()); myMap.put(entry.getKey(), entry.getValue()); }
	 * 
	 * for ( Integer value : myMap.values() ) {
	 * Console.sendMessageDebug("Result map values: " + value); } for (
	 * Map.Entry<String,Integer> entry : myMap.entrySet() ) {
	 * Console.sendMessageDebug("Result map entries: " + entry.getKey() + " -> " +
	 * entry.getValue()); } return myMap;
	 */

	public static int getTeamSize() {
		List<Team> teams = new ArrayList<>();
		for (String playerName : playersInGame) {
			UPlayer uplayers = players.get(playerName);
			Team team = uplayers.team;
			if (team != null && team.isInTeam(playerName) && !(teams.contains(team)))
				teams.add(team);
		}
		return teams.size();
	}

	public static World getGameWorld() {
		return Bukkit.getWorld("uhc");
	}

	public static World getLobbyWorld() {
		return Bukkit.getWorld("uhc");
	}
}
