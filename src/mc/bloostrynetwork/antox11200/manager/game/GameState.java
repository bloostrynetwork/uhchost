package mc.bloostrynetwork.antox11200.manager.game;

import mc.bloostrynetwork.antox11200.bloostryapi.utils.rank.Ranks;

public enum GameState {
	
	//PREGENERATORWORLD("G�n�ration du monde", true, 9999999999F, "�cChargement de la map en cours ...", Step.OTHER),
	HOST("Pr�paration de la partie", true, Ranks.HOST.getBloostryRank().getPermissionPower(), "�CPr�paration de la partie", Step.INWAITING),
	PRE_WHITELIST("Pr�-whitelist", true, 50L, "�CVous n'avez pas de pr�-whitelist", Step.INWAITING),
	GLOBAL_WAITING("Attente de joueur", false, 50L, "�cLe partie est d�j� en cours !", Step.INWAITING),
	TELEPORTING("T�l�portation", true, 700L, "�CLe partie est d�j� en cours !", Step.INGAME),
	FARMING("Pr�paration", true, Ranks.HOST.getBloostryRank().getPermissionPower(), "�CLa partie est d�j� en cours !", Step.INGAME), 
	PVP("Combat", true, Ranks.HOST.getBloostryRank().getPermissionPower(), "Le partie est d�j� en cours !", Step.INGAME), 
	BORDER("Bordure", true, Ranks.HOST.getBloostryRank().getPermissionPower(), "Le partie est d�j� en cours !", Step.INGAME),  
	FINISH("Fin de partie", true, Ranks.HOST.getBloostryRank().getPermissionPower(), "�CLa partie est termin�e !", Step.INGAME),
	RESET("R�initialisation", true, 99999L, "�CLe serveur red�marre !", Step.INWAITING);
		
	public String name;
	public boolean requestRankLevel;
	public Long level;
	public String errorMessage;
	public Step stade;
	
	GameState(String name, boolean requestRankLevel, Long level, String errorMessage, Step stade){
		this.name = name;
		this.requestRankLevel = requestRankLevel;
		this.level = level;
		this.errorMessage = errorMessage;
		this.stade = stade;
	}
}
