package mc.bloostrynetwork.antox11200.manager.game.team;

import org.bukkit.ChatColor;

public enum TeamColor {

	GREEN(false, ChatColor.GREEN),
	RED(false, ChatColor.RED),
	YELLOW(false, ChatColor.YELLOW),
	BLUE(false, ChatColor.BLUE),
	GRAY(false, ChatColor.GRAY),
	WHITE(false, ChatColor.WHITE),
	DARK_PURPLE(false, ChatColor.DARK_PURPLE),
	DARK_GRAY(false, ChatColor.DARK_GRAY),
	DARK_GREEN(false, ChatColor.DARK_GREEN),
	DARK_BLUE(false, ChatColor.DARK_BLUE);
	
	private Boolean used;
	private ChatColor color;
	
	private TeamColor(Boolean used, ChatColor color) {
		this.used = used;
		this.color = color;
	}

	public Boolean getUsed() {
		return used;
	}

	public void setUsed(Boolean used) {
		this.used = used;
	}

	public ChatColor getColor() {
		return color;
	}

	public void setColor(ChatColor color) {
		this.color = color;
	}
}
