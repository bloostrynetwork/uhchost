package mc.bloostrynetwork.antox11200.manager.game.team;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import mc.bloostrynetwork.antox11200.manager.game.GameManager;
import mc.bloostrynetwork.antox11200.player.UPlayer;

public class Team {

	private static TeamCarractere teamC = null;

	private TeamColor teamColor = null;
	private Player owner;
	private List<String> players = new ArrayList<>();
	private List<String> playersDeath = new ArrayList<>();
	private String prefix = "�cnull";
	
	public Location location;

	public Team(Player owner) {
		this.owner = owner;

		Boolean result = false;

		for (TeamColor teamsColor : TeamColor.values()) {
			if (!result) {
				if (!teamsColor.getUsed()) {
					this.teamColor = teamsColor;
					teamsColor.setUsed(true);
					result = true;
				}
			}
		}

		if (!result) {
			teamC.setUsed(true);
			teamC = null;
			for (TeamColor teamsColor : TeamColor.values()) {
				teamsColor.setUsed(false);
				teamColor = TeamColor.GREEN;
				teamColor.setUsed(true);
			}
		}

		Boolean repeat = false;

		for (TeamCarractere teams : TeamCarractere.values()) {
			if (!repeat) {
				if (teamC == null) {
					if (!teams.getUsed()) {
						repeat = true;
						teamC = teams;
					}
				}
			}
		}

		this.prefix = teamColor.getColor() + teamC.getPrefix();

		this.players.add(owner.getName());
	}

	public void addPlayer(String player) {
		if (!players.contains(player)) {
			this.players.add(player);
		}
	}

	public void removePlayer(String player) {
		if (players.contains(player)) {
			this.players.remove(player);
		}
	}

	public void addPlayerDeath(String player) {
		if (!playersDeath.contains(player)) {
			this.playersDeath.add(player);
		}
	}

	public void removePlayerDeath(String player) {
		if (playersDeath.contains(player)) {
			this.playersDeath.remove(player);
		}
	}

	public void delete() {
		for (String players : getPlayers()) {
				UPlayer pls = GameManager.players.get(players);
				pls.team = (null);
		}
		getPlayers().clear();

		if (teamC != null && teamColor != null) {
			if (!teamC.getUsed()) {
				teamColor.setUsed(false);
			}
		}
	}

	public List<String> getPlayers() {
		return this.players;
	}

	public Boolean isInTeam(String player) {
		return this.players.contains(player);
	}

	public Player getOwner() {
		return owner;
	}

	public void setOwner(Player owner) {
		this.owner = owner;
	}

	public String getPrefix() {
		return this.prefix;
	}
}
