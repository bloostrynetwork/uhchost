package mc.bloostrynetwork.antox11200.manager.game.team;

public enum TeamType {

	FFA("FFA", 1), TO2("TO2", 2), TO3("TO3", 3), TO5("TO5", 5), TO10("TO10", 10);

	public String name;
	public int maxPlayers;

	TeamType(String name, int maxPlayers) {
		this.name = name;
		this.maxPlayers = maxPlayers;
	}

}
