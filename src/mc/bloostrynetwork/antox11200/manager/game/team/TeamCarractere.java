package mc.bloostrynetwork.antox11200.manager.game.team;

public enum TeamCarractere {

	a(false, "✦"),
	z(false, "❂"),
	e(false, "✳"),
	r(false, "⚜"),
	t(false, "✺"),
	y(false, "✻"),
	u(false, "✼"),
	i(false, "✡"),
	o(false, "❁"),
	p(false, "✿"),
	q(false, "✾"),
	s(false, "☃"),
	d(false, "⚙"),
	f(false, "✴"),
	g(false, "☆"),
	h(false, "⚖"),
	j(false, "❅"),
	k(false, "♽"),
	l(false, "♼"),
	m(false, "✽"),
	w(false, "✞"),
	x(false, "π"),
	c(false, "∞"),
	v(false, "Σ");
	
	private Boolean used;
	private String prefix;
	
	private TeamCarractere(Boolean used, String prefix) {
		this.used = used;
		this.prefix = prefix;
	}

	public Boolean getUsed() {
		return used;
	}

	public void setUsed(Boolean used) {
		this.used = used;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
}
