package mc.bloostrynetwork.antox11200.manager;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import mc.bloostrynetwork.antox11200.listeners.BlockListener;
import mc.bloostrynetwork.antox11200.listeners.EntityDamageByEntityListener;
import mc.bloostrynetwork.antox11200.listeners.EntityRegainHealthListener;
import mc.bloostrynetwork.antox11200.listeners.EntitySpawnListener;
import mc.bloostrynetwork.antox11200.listeners.InventoryClickListener;
import mc.bloostrynetwork.antox11200.listeners.PlayerArmorStandManipulateListener;
import mc.bloostrynetwork.antox11200.listeners.PlayerChatListener;
import mc.bloostrynetwork.antox11200.listeners.PlayerCommandPreprocessListener;
import mc.bloostrynetwork.antox11200.listeners.PlayerConnectListener;
import mc.bloostrynetwork.antox11200.listeners.PlayerDeathListener;
import mc.bloostrynetwork.antox11200.listeners.PlayerFoodLevelChangeListener;
import mc.bloostrynetwork.antox11200.listeners.PlayerMoveListener;
import mc.bloostrynetwork.antox11200.listeners.PrepareItemCraftListener;
import mc.bloostrynetwork.antox11200.manager.game.rules.RulesListeners;
import mc.bloostrynetwork.antox11200.manager.game.scenarios.depends.EventsListeners;

public class EventsManager {

	public JavaPlugin instance;

	public EventsManager(JavaPlugin instance) {
		this.instance = instance;
	}
	
	public void registerListeners() {
		PluginManager pm = Bukkit.getPluginManager();
		pm.registerEvents(new PlayerConnectListener(), instance);
		pm.registerEvents(new InventoryClickListener(), instance);
		pm.registerEvents(new EntityDamageByEntityListener(), instance);
		pm.registerEvents(new PlayerFoodLevelChangeListener(), instance);
		pm.registerEvents(new EventsListeners(), instance);
		pm.registerEvents(new RulesListeners(), instance);
		pm.registerEvents(new PlayerDeathListener(), instance);
		pm.registerEvents(new BlockListener(), instance);
		pm.registerEvents(new EntityRegainHealthListener(), instance);
		pm.registerEvents(new PlayerMoveListener(), instance);
		pm.registerEvents(new PlayerArmorStandManipulateListener(), instance);
		pm.registerEvents(new PlayerCommandPreprocessListener(), instance);
		pm.registerEvents(new PrepareItemCraftListener(), instance);
		pm.registerEvents(new PlayerChatListener(), instance);
		pm.registerEvents(new EntitySpawnListener(), instance);
	}
}

