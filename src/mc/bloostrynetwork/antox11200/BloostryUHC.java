package mc.bloostrynetwork.antox11200;

import java.lang.reflect.Field;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import mc.bloostrynetwork.antox11200.bloostryapi.BloostryAPI;
import mc.bloostrynetwork.antox11200.bloostryapi.Main;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.console.Console;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.player.BloostryPlayer;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.rank.Ranks;
import mc.bloostrynetwork.antox11200.commands.ReviveCommand;
import mc.bloostrynetwork.antox11200.commands.TeamCommand;
import mc.bloostrynetwork.antox11200.commands.UHCHostCommand;
import mc.bloostrynetwork.antox11200.commands.sayCommand;
import mc.bloostrynetwork.antox11200.commands.specCommand;
import mc.bloostrynetwork.antox11200.commands.voteCommand;
import mc.bloostrynetwork.antox11200.manager.EventsManager;
import mc.bloostrynetwork.antox11200.manager.game.GameManager;
import mc.bloostrynetwork.antox11200.manager.game.GameState;
import mc.bloostrynetwork.antox11200.manager.game.runnable.RunnableManager;
import mc.bloostrynetwork.antox11200.player.PlayerScoreboard;
import mc.bloostrynetwork.antox11200.player.UPlayer;
import net.minecraft.server.v1_8_R3.BiomeBase;

public class BloostryUHC extends JavaPlugin {

	private static BloostryUHC instance;
	
	public void onLoad() {
		long timeLast;
		try {
			Field biomesField = BiomeBase.class.getDeclaredField("biomes");
			biomesField.setAccessible(true);
			System.out.println("Starting changer biomes");
			timeLast = System.currentTimeMillis();
			if (biomesField.get(null) instanceof BiomeBase[]) {
				BiomeBase[] biomes = (BiomeBase[]) biomesField.get(null);
				for (BiomeBase biome : biomes) {
					if (biome == null)
						return;
					int id = biome.id;
					BiomeBase biomeFind = biomes[id];
					if (biomeFind == BiomeBase.OCEAN || biomeFind == BiomeBase.DEEP_OCEAN
							|| biomeFind == BiomeBase.FROZEN_OCEAN) {
						biomeFind = BiomeBase.FOREST;
						System.out.println("[" + GameManager.PREFIX + "] [BiomeChanger] biome["
								+ biomeFind.getClass().getSimpleName() + "] changed for Forest");
					} else if (biomeFind == BiomeBase.JUNGLE || biomeFind == BiomeBase.JUNGLE_EDGE
							|| biomeFind == BiomeBase.JUNGLE_HILLS) {
						biomeFind = BiomeBase.FOREST;
						System.out.println("[" + GameManager.PREFIX + "] [BiomeChanger] biome["
								+ biomeFind.getClass().getSimpleName() + "] is changed for Forest");
					} else if (biomeFind == BiomeBase.DESERT || biomeFind == BiomeBase.DESERT_HILLS) {
						biomeFind = BiomeBase.FOREST;
						System.out.println("[" + GameManager.PREFIX + "] [BiomeChanger] biome["
								+ biomeFind.getClass().getSimpleName() + "] is changed for Forest");
					} else if (biomeFind == BiomeBase.SWAMPLAND) {
						biomeFind = BiomeBase.FOREST;
						System.out.println("[" + GameManager.PREFIX + "] [BiomeChanger] biome["
								+ biomeFind.getClass().getSimpleName() + "] is changed for Forest");
					} else if (biomeFind == BiomeBase.COLD_TAIGA_HILLS || biomeFind == BiomeBase.TAIGA
							|| biomeFind == BiomeBase.TAIGA_HILLS || biomeFind == BiomeBase.COLD_TAIGA
							|| biomeFind == BiomeBase.MEGA_TAIGA || biomeFind == BiomeBase.MEGA_TAIGA_HILLS) {
						biomeFind = BiomeBase.FOREST;
						System.out.println("[" + GameManager.PREFIX + "] [BiomeChanger] biome["
								+ biomeFind.getClass().getSimpleName() + "] is changed for Forest");
					}
					biomes[id] = biomeFind;
				}
			}
			System.out.println("Stopping change biomes (latency: " + (System.currentTimeMillis() - timeLast) + "ms)");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void onEnable() {
		instance = this;

		new RunnableManager().loadAllRunnable();

		GameManager.init();

		new EventsManager(this).registerListeners();

		BloostryUHC.getInstance().getConfig().options().copyDefaults(true);
		BloostryUHC.getInstance().saveConfig();

		Main.bloostryAPI.getCommandsManager().addCommand(this, new UHCHostCommand());
		Main.bloostryAPI.getCommandsManager().addCommand(this, new TeamCommand());	
		Main.bloostryAPI.getCommandsManager().addCommand(this, new ReviveCommand());	
		Main.bloostryAPI.getCommandsManager().addCommand(this, new specCommand());	
		Main.bloostryAPI.getCommandsManager().addCommand(this, new voteCommand());
		Main.bloostryAPI.getCommandsManager().addCommand(this, new sayCommand());	
		Main.bloostryAPI.getCommandsManager().addCommand(this, new ReviveCommand());	
		Main.bloostryAPI.getCommandsManager().registerCommands();

		Console.sendMessageInfo("�7�M----------------------------------------------");
		Console.sendMessageInfo("");
		Console.sendMessageInfo("		" + GameManager.PREFIX);
		Console.sendMessageInfo("");
		Console.sendMessageInfo("�6Main: �e" + getDescription().getMain());
		Console.sendMessageInfo("�6Author: �e" + getDescription().getAuthors().get(0));
		Console.sendMessageInfo("�6Version: �e" + getDescription().getVersion());
		Console.sendMessageInfo("");
		Console.sendMessageInfo("�aPlugin enabled");
		Console.sendMessageInfo("");
		Console.sendMessageInfo("�7�M----------------------------------------------");

		GameManager.gameState = GameState.HOST;
		
		reload();
	}

	public void onDisable() {
		GameManager.stop();

		new RunnableManager().unloadAllRunnable();

		Console.sendMessageInfo("�7�M----------------------------------------------");
		Console.sendMessageInfo("");
		Console.sendMessageInfo("		" + GameManager.PREFIX);
		Console.sendMessageInfo("");
		Console.sendMessageInfo("�6Main: �e" + getDescription().getMain());
		Console.sendMessageInfo("�6Author: �e" + getDescription().getAuthors().get(0));
		Console.sendMessageInfo("�6Version: �e" + getDescription().getVersion());
		Console.sendMessageInfo("");
		Console.sendMessageInfo("�cPlugin disabled");
		Console.sendMessageInfo("");
		Console.sendMessageInfo("�7�M----------------------------------------------");
	}

	public static boolean hasGenerateWorldOnStartup() {
		return BloostryUHC.getInstance().getConfig().getBoolean("generate-world-on-startup-plugin");
	}

	public void reload() {
		for (Player player : Bukkit.getOnlinePlayers()) {
			BloostryPlayer iplayer = BloostryAPI.getBloostryPlayer(player.getUniqueId());
			if (iplayer.getBloostryRank().getPermissionPower() <= Ranks.HOST.getBloostryRank().getPermissionPower()) {
				GameState gameState = GameManager.gameState;
				player.kickPlayer("�7Erreur de connexion sur le serveur \n" + gameState.errorMessage);
			} else {
				GameManager.players.put(player.getName(),
						new UPlayer(player.getName(), player.getUniqueId(), iplayer));

				Bukkit.broadcastMessage(GameManager.PREFIX + "�e" + player.getName() + " �6rejoint la partie �8(�e"
						+ GameManager.players.size() + "�7/�e" + GameManager.MAX_PLAYERS + "�8)�e.");

				UPlayer uPlayer = GameManager.players.get(player.getName());
				uPlayer.playerScoreboard = new PlayerScoreboard(uPlayer);
				uPlayer.playerScoreboard.create();
				uPlayer.joinInWaiting();
			}
		}
	}

	public static BloostryUHC getInstance() {
		return instance;
	}
}
