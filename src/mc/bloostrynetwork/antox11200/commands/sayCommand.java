package mc.bloostrynetwork.antox11200.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import mc.bloostrynetwork.antox11200.bloostryapi.BloostryAPI;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.commands.ICommand;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.player.BloostryPlayer;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.rank.Ranks;
import mc.bloostrynetwork.antox11200.manager.game.GameManager;

public class sayCommand implements ICommand {

	@Override
	public String getCommand() {
		return "say";
	}

	@Override
	public TabCompleter getTabCompleter() {
		return null;
	}

	@Override
	public void handle(CommandSender commandSender, String[] args) {
		if (commandSender instanceof Player) {
			BloostryPlayer bloostryPlayer = BloostryAPI.getBloostryPlayer(((Player) commandSender).getUniqueId());
			if (bloostryPlayer != null) {
				if (bloostryPlayer.getBloostryRank().getPermissionPower() < Ranks.HOST.getBloostryRank()
						.getPermissionPower()) {
					commandSender.sendMessage(
							GameManager.PREFIX + "�cVous n'avez pas le rang requis pour �x�cuter cette commande !");
					return;
				}
			} else {
				commandSender.sendMessage(GameManager.PREFIX + "�cVous n'�tes dans la base de donn�e !");
				return;
			}
		}
		String text = "";
		for(int i = 0; i < args.length; i++) {
			text += args[i]+ " ";
		}
		Bukkit.broadcastMessage("�8[�6Host�8] �e� �6�l"+text);
	}
}

