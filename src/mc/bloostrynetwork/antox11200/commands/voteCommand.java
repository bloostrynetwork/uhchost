package mc.bloostrynetwork.antox11200.commands;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import mc.bloostrynetwork.antox11200.BloostryUHC;
import mc.bloostrynetwork.antox11200.bloostryapi.BloostryAPI;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.commands.ICommand;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.player.BloostryPlayer;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.rank.Ranks;
import mc.bloostrynetwork.antox11200.manager.game.GameManager;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;
import net.minecraft.server.v1_8_R3.IChatBaseComponent.ChatSerializer;

public class voteCommand implements ICommand {

	public static boolean hasVote;
	public static String textVote;
	public static ArrayList<String> voteYes = new ArrayList<>();
	public static ArrayList<String> voteNo = new ArrayList<>();

	@Override
	public String getCommand() {
		return null;
	}

	@Override
	public TabCompleter getTabCompleter() {
		return null;
	}

	@Override
	public void handle(CommandSender commandSender, String[] args) {
		if (commandSender instanceof Player) {
			BloostryPlayer bloostryPlayer = BloostryAPI.getBloostryPlayer(((Player) commandSender).getUniqueId());
			if (bloostryPlayer != null) {
				if (bloostryPlayer.getBloostryRank().getPermissionPower() < Ranks.HOST.getBloostryRank()
						.getPermissionPower()) {
					commandSender.sendMessage(
							GameManager.PREFIX + "�cVous n'avez pas le rang requis pour �x�cuter cette commande !");
					return;
				}
			} else {
				commandSender.sendMessage(GameManager.PREFIX + "�cVous n'�tes dans la base de donn�e !");
				return;
			}
		}
		if (args.length > 1) {
			if (!hasVote) {
				String text = "";
				for (int i = 0; i < args.length; i++) {
					text += args[i] + " ";
				}

				hasVote = true;
				textVote = text;

				Bukkit.broadcastMessage("�7�m-----------------------------------");
				Bukkit.broadcastMessage("");
				Bukkit.broadcastMessage("�6" + textVote);
				Bukkit.broadcastMessage("");
				Bukkit.broadcastMessage("�6Le vote se termine dans 1 minute");
				Bukkit.broadcastMessage("");
				
				for(Player players : Bukkit.getOnlinePlayers()) {
					PacketPlayOutChat chat = new PacketPlayOutChat(ChatSerializer
							.a("{\"text\":\""+GameManager.PREFIX_TEAM+" �AOui �7(Clic pour voter)\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/vote yes \"\"}}"));

					((CraftPlayer) players).getHandle().playerConnection.sendPacket(chat);

					chat = new PacketPlayOutChat(ChatSerializer
							.a("{\"text\":\""+GameManager.PREFIX_TEAM+" �CNon �7(Clic pour voter)\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/vote no\"\"}}"));

					((CraftPlayer) players).getHandle().playerConnection.sendPacket(chat);	
				}
				Bukkit.broadcastMessage("");
				Bukkit.broadcastMessage("�7�m-----------------------------------");

				Bukkit.getScheduler().runTaskLater(BloostryUHC.getInstance(), new Runnable() {
					@Override
					public void run() {
						Bukkit.broadcastMessage("�7�m-----------------------------------");
						Bukkit.broadcastMessage("");
						Bukkit.broadcastMessage("�6" + textVote);
						Bukkit.broadcastMessage("");
						Bukkit.broadcastMessage("�6Le vote est termin�");
						Bukkit.broadcastMessage("");
						Bukkit.broadcastMessage("�7Reponse:");
						Bukkit.broadcastMessage("  �aOui: " + voteYes.size());
						Bukkit.broadcastMessage("  �cNon: " + voteNo.size());
						Bukkit.broadcastMessage("");
						Bukkit.broadcastMessage("�7�m-----------------------------------");
						hasVote = false;
						textVote = "";
					}
				}, 20 * 60);
			} else {
				commandSender.sendMessage(GameManager.PREFIX + "�cUn vote a d�j� �t� cr�� !");
			}
		} else {
			String arg1 = args[0];
			if (arg1.equalsIgnoreCase("yes")) {
				if(hasVote) {
					if(!voteYes.contains(commandSender.getName()) && !voteNo.contains(commandSender.getName())) {
						voteYes.add(commandSender.getName());
						commandSender.sendMessage(GameManager.PREFIX+"�aVotre vote vient d'�tre envoy�.");
					} else {
						commandSender.sendMessage(GameManager.PREFIX+"�cVous avez d�j� vot� !");
					}
				} else {
					commandSender.sendMessage("�cAucun vote n'a �t� cr�� !");
				}
			} else if (arg1.equalsIgnoreCase("no")) {
				if(hasVote) {
					if(!voteYes.contains(commandSender.getName()) && !voteNo.contains(commandSender.getName())) {
						voteNo.add(commandSender.getName());
						commandSender.sendMessage(GameManager.PREFIX+"�aVotre vote vient d'�tre envoy�.");
					} else {
						commandSender.sendMessage(GameManager.PREFIX+"�cVous avez d�j� vot� !");
					}
				} else {
					commandSender.sendMessage("�cAucun vote n'a �t� cr�� !");
				}
			} else {
				commandSender.sendMessage(GameManager.PREFIX+"�cErreur, essayez: /vote <yes/no>");
			}
		}
	}
}
