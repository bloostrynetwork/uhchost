package mc.bloostrynetwork.antox11200.commands;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import mc.bloostrynetwork.antox11200.bloostryapi.utils.commands.ICommand;
import mc.bloostrynetwork.antox11200.manager.game.GameManager;
import mc.bloostrynetwork.antox11200.manager.game.Step;
import mc.bloostrynetwork.antox11200.manager.game.team.Team;
import mc.bloostrynetwork.antox11200.player.UPlayer;
import net.minecraft.server.v1_8_R3.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;

public class TeamCommand implements ICommand {

	@Override
	public String getCommand() {
		return "team";
	}

	@Override
	public TabCompleter getTabCompleter() {
		return null;
	}

	private Map<Player, Player> request = new HashMap<>();

	@Override
	public void handle(CommandSender sender, String[] args) {
		if (sender instanceof Player) {
			Player p = (Player) sender;
			UPlayer uplayer = GameManager.players.get(p.getName());

			if (args.length == 0) {
				p.sendMessage("�7�m----------------------------------");
				p.sendMessage("");
				p.sendMessage("�eListe des commandes:");
				p.sendMessage("�6- �7/team create");
				p.sendMessage("�6- �7/team delete");
				p.sendMessage("�6- �7/team invite <joueur>");
				p.sendMessage("�6- �7/team accept <joueur>");
				p.sendMessage("�6- �7/team kick <joueur>");
				p.sendMessage("�6- �7/team leave");
				p.sendMessage("�6- �7/team list <joueur>");
				p.sendMessage("");
				p.sendMessage("�7�m----------------------------------");
			} else {
				if (args[0].equalsIgnoreCase("list")) {
					if (args.length == 2) {
						Player target = Bukkit.getPlayer(args[1]);
						if (target != null) {
							UPlayer utarget = GameManager.players.get(target.getName());
							if (utarget.team != null) {
								p.sendMessage("�7�m----------------------------------------------");
								p.sendMessage("");
								p.sendMessage("�6Liste des joueurs de l'�quipe de " + utarget.team.getOwner().getName()
										+ "�6:");
								for (String players : utarget.team.getPlayers()) {
									p.sendMessage("�6- �7" + players.replace(utarget.team.getOwner().getName(),
											"�c" + utarget.team.getOwner().getName()));
								}
								p.sendMessage("");
								p.sendMessage("�7�m----------------------------------------------");
							} else {
								p.sendMessage(GameManager.PREFIX_TEAM + "�cCe joueur n'a pas d'�quipe !");
							}
						} else {
							p.sendMessage(GameManager.PREFIX_TEAM + "�cCe joueur n'est pas connect� !");
						}
					}
				}
				if (!GameManager.hasTeam()) {
					p.sendMessage(GameManager.PREFIX_TEAM + "�cLes �quipes ne sont pas activ�es !");
					return;
				}
				if (GameManager.gameState.stade == Step.INGAME) {
					p.sendMessage(GameManager.PREFIX_TEAM
							+ "�cVous ne pouvez pas faire ces commandes quand la partie est lanc� !");
					return;
				}
				if (args[0].equalsIgnoreCase("create")) {
					if (!(uplayer.team != null)) {
						Team team = new Team(p);
						uplayer.team = (team);
						p.sendMessage(GameManager.PREFIX_TEAM + "�6Vous venez de cr�er votre �quipe.");
						p.sendMessage(GameManager.PREFIX_TEAM
								+ "�e/team invite <joueur> �6| Pour inviter des joueurs !");
					} else {
						p.sendMessage(GameManager.PREFIX_TEAM + "�cVous avez d�j� une �quipe !");
					}
				} else if (args[0].equalsIgnoreCase("delete")) {
					if (uplayer.team != null) {
						if (uplayer.team.getOwner().getName().equals(p.getName())) {
							for (String players : uplayer.team.getPlayers()) {
								Player pls = GameManager.players.get(players).getPlayer();
								if (!uplayer.team.getOwner().getName().equals(pls.getName()))
									pls.sendMessage(GameManager.PREFIX_TEAM + "�e" + p.getName()
											+ " �cvient de supprimer l'�quipe !");
							}
							uplayer.team.delete();
							p.sendMessage(GameManager.PREFIX_TEAM + "�6Vous avez supprim� votre �quipe !");
						} else {
							p.sendMessage(GameManager.PREFIX_TEAM + "�cVous n'�tes pas le cr�ateur de cette �quipe !");
						}
					} else {
						p.sendMessage(GameManager.PREFIX_TEAM + "�cVous n'avez pas d'�quipe !");
					}
				} else if (args[0].equalsIgnoreCase("invite")) {
					if (args.length == 2) {
						if (uplayer.team != null) {
							if (uplayer.team.getOwner().getName().equals(p.getName())) {
								Player target = Bukkit.getPlayer(args[1]);
								if (target != null) {
									UPlayer utarget = GameManager.players.get(target.getName());
									if (!(utarget.team != null)) {
										if (uplayer.team.getPlayers().size() >= GameManager.teamType.maxPlayers) {
											p.sendMessage(GameManager.PREFIX_TEAM + "�cCette �quipe est compl�te !");
											return;
										}
										
										PacketPlayOutChat chat = new PacketPlayOutChat(ChatSerializer
												.a("{\"text\":\""+GameManager.PREFIX_TEAM+" �6Vous �tes invit� dans l'�quipe de �e" + p.getName()
														+ "�6. �6(�eClic pour rejoindre�6)\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/team accept "
														+ p.getName() + "\"}}"));

										((CraftPlayer) target).getHandle().playerConnection.sendPacket(chat);

										p.sendMessage(GameManager.PREFIX_TEAM + "�6Vous avez envoy� une invitation � �e"
												+ target.getName() + "�6.");
										request.put(p, target);
									} else {
										p.sendMessage(
												GameManager.PREFIX_TEAM + "�cCe joueur est d�j� dans une �quipe !");
									}
								} else {
									p.sendMessage(GameManager.PREFIX_TEAM + "�cCe joueur n'est pas connect� !");
								}
							} else {
								p.sendMessage(
										GameManager.PREFIX_TEAM + "�cVous n'�tes pas le cr�ateur de cette �quipe !");
							}
						} else {
							p.sendMessage(GameManager.PREFIX_TEAM + "�cVous n'avez pas d'�quipe !");
						}
					}
				} else if (args[0].equalsIgnoreCase("accept")) {
					if (args.length == 2) {
						if (!(uplayer.team != null)) {
							Player target = Bukkit.getPlayer(args[1]);
							if (target != null) {
								UPlayer utarget = GameManager.players.get(target.getName());
								if (utarget.team != null) {
									if (request.containsKey(target)) {
										if (request.get(target).equals(p)) {
											request.remove(target);
											if (utarget.team.getPlayers().size() >= GameManager.teamType.maxPlayers) {
												p.sendMessage(
														GameManager.PREFIX_TEAM + "�cCette �quipe est compl�te !");
												return;
											}
											for (String players : utarget.team.getPlayers()) {
												UPlayer pls = GameManager.players.get(players);
												if (pls.isOnline()) {
													pls.getPlayer().sendMessage(GameManager.PREFIX_TEAM + "�e"
															+ p.getName() + " �6vient de rejoindre l'�quipe !");
												}
											}
											uplayer.team = (utarget.team);
											uplayer.team.addPlayer(p.getName());
											p.sendMessage(GameManager.PREFIX_TEAM + "�6Vous avez rejoint l'�quipe de �e"
													+ target.getName() + "�6.");
										} else {
											p.sendMessage(GameManager.PREFIX_TEAM
													+ "�cCe joueur ne vous a pas envoy� de demande d'invitation !");
										}
									} else {
										p.sendMessage(GameManager.PREFIX_TEAM
												+ "�cCe joueur ne vous a pas envoy� de demande d'invitation !");
									}
								} else {
									p.sendMessage(GameManager.PREFIX_TEAM + "�cCe joueur n'a pas d'�quipe !");
								}
							} else {
								p.sendMessage(GameManager.PREFIX_TEAM + "�cCe joueur n'est pas connect� !");
							}
						} else {
							p.sendMessage(GameManager.PREFIX_TEAM + "�cVous avez d�j� une �quipe !");
						}
					}
				} else if (args[0].equals("leave")) {
					if (args.length == 1) {
						if (uplayer.team != null) {
							if (!(uplayer.team.getOwner().getName().equals(p.getName()))) {
								for (String players : uplayer.team.getPlayers()) {
									Player pls = GameManager.players.get(players).getPlayer();
									if (pls.isOnline()) {
										if (!uplayer.getPlayer().getName().equals(pls.getName())) {
											pls.sendMessage(GameManager.PREFIX_TEAM + "�e" + p.getName()
													+ " �cvient de quitter l'�quipe !");
										}
									}
								}
								String owner = uplayer.team.getOwner().getName();
								p.sendMessage(
										GameManager.PREFIX_TEAM + "�cVous avez quitt� l'�quipe de �e" + owner + "�c.");
								uplayer.team.removePlayer(p.getName());
								uplayer.team = (null);
							} else {
								p.sendMessage(
										GameManager.PREFIX_TEAM + "�cVous ne pouvez pas quitter votre propre �quipe !");
							}
						} else {
							p.sendMessage(GameManager.PREFIX_TEAM + "�cVous n'avez pas d'�quipe !");
						}
					}
				} else if (args[0].equals("kick")) {
					if (args.length == 2) {
						if (uplayer.team != null) {
							if (uplayer.team.getOwner().getName().equals(p.getName())) {
								Player target = Bukkit.getPlayer(args[1]);
								if (target != null) {
									UPlayer utarget = GameManager.players.get(target.getName());
									if (utarget.team != null) {
										if (utarget.team.getOwner().getName()
												.equals(uplayer.team.getOwner().getName())) {
											for (String players : utarget.team.getPlayers()) {
												Player pls = GameManager.players.get(players).getPlayer();
												if (pls.isOnline()) {
													if (!(pls.getName().equals(utarget.team.getOwner().getName())
															|| target.getName().equals(pls.getName())))
														pls.sendMessage(
																GameManager.PREFIX_TEAM + "�e" + target.getName()
																		+ " �cvient d'�tre �ject� de l'�quipe !");
												}
											}
											p.sendMessage(GameManager.PREFIX_TEAM + "�cVous avez �ject� le joueur �e"
													+ target.getName() + "�c !");
											target.sendMessage(GameManager.PREFIX_TEAM
													+ "�cVous avez �t� �ject� de l'�quipe de �e" + p.getName() + "�c.");
											utarget.team.removePlayer(target.getName());
											utarget.team = (null);
										} else {
											p.sendMessage(GameManager.PREFIX_TEAM
													+ "�cCe joueur n'est pas dans votre �quipe !");
										}
									} else {
										p.sendMessage(GameManager.PREFIX_TEAM + "�cCe joueur n'a pas d'�quipe !");
									}
								} else {
									p.sendMessage(GameManager.PREFIX_TEAM + "�cCe joueur n'est pas connect� !");
								}
							} else {
								p.sendMessage(
										GameManager.PREFIX_TEAM + "�cVous n'�tes pas le cr�ateur de cette �quipe !");
							}
						} else {
							p.sendMessage(GameManager.PREFIX_TEAM + "�cVous n'avez pas d'�quipe !");
						}
					}
				}
			}
		}
	}
}
