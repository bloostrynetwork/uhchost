package mc.bloostrynetwork.antox11200.commands;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import mc.bloostrynetwork.antox11200.bloostryapi.BloostryAPI;
import mc.bloostrynetwork.antox11200.bloostryapi.Main;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.DateUtils;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.commands.ICommand;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.player.BloostryPlayer;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.rank.Ranks;
import mc.bloostrynetwork.antox11200.manager.game.GameManager;
import mc.bloostrynetwork.antox11200.manager.game.GameState;
import mc.bloostrynetwork.antox11200.manager.game.Step;
import mc.bloostrynetwork.antox11200.manager.game.rules.RulesManager;
import mc.bloostrynetwork.antox11200.manager.game.scenarios.depends.Scenarios;
import twitter4j.TwitterException;

public class UHCHostCommand implements ICommand {

	private static final String COMMAND_INVALIDE = GameManager.PREFIX
			+ "§cLa commande que vous venez d'entrer n'est pas valide !";
	private static final String PLEASE_SET_NUMBER = GameManager.PREFIX + "§CErreur, veuillez définir un nombre !";

	private static boolean hasTweeted = false;

	@Override
	public String getCommand() {
		return "uhchost";
	}

	@Override
	public void handle(CommandSender commandSender, String[] args) {
		if (args.length == 0) {
			commandSender.sendMessage("§7§m--------------------------------------");
			commandSender.sendMessage("");
			commandSender.sendMessage("§eListe des commandes du plugin !");
			commandSender.sendMessage("§6- §7/uhchost tweet <heure>");
			commandSender.sendMessage("§6- §7/uhchost start");
			commandSender.sendMessage("§6- §7/uhchost test <options> <variables>");
			commandSender.sendMessage("§6- §7/uhchost forcestart | pour forcer le démarrage");
			commandSender.sendMessage("§6- §7/uhchost stop <raison>");
			commandSender.sendMessage("§6- §7/uhchost restart");
			commandSender.sendMessage("§6- §7/uhchost open <whitelist/all>");
			commandSender.sendMessage("§6- §7/uhchost config");
			commandSender.sendMessage("§6- §7/uhchost pre-whitelist add <joueur> <nombre>");
			commandSender.sendMessage("§6- §7/uhchost pre-whitelist remove <joueur> <nombre>");
			commandSender.sendMessage("");
			commandSender.sendMessage("§7§m--------------------------------------");
		} else {
			String arg1 = args[0];
			if(arg1.equalsIgnoreCase("rules")) {

				Bukkit.broadcastMessage("§7§M--------------------------------");
				Bukkit.broadcastMessage("");
				Bukkit.broadcastMessage("§6Host: §e" + (GameManager.HOST_NAME == null ? "inconnu" : GameManager.HOST_NAME));
				Bukkit.broadcastMessage("");
				Bukkit.broadcastMessage("§6Scénarios:");
				for (Scenarios scenarios : Scenarios.values()) {
					if (scenarios.iScenario.isEnabled()) {
						Bukkit.broadcastMessage("  §8- §e" + scenarios.iScenario.getName());
					}
				}
				Bukkit.broadcastMessage("");
				Bukkit.broadcastMessage("§6Règles:");
				Bukkit.broadcastMessage(
						"  §6Golden Head: " + (RulesManager.GOLDENHEAD ? "§aActivées" : "§cDésactivées"));
				Bukkit.broadcastMessage("  §6Cheveaux: " + (RulesManager.HORSE ? "§aActivés" : "§cDésactivés"));
				Bukkit.broadcastMessage("  §6Nether: " + (RulesManager.NETHER ? "§aActivé" : "§cDésactivé"));
				Bukkit.broadcastMessage("");
				Bukkit.broadcastMessage("  §6Taille de bordure: §e"+GameManager.BORDER_SIZE+"§8x§e"+GameManager.BORDER_SIZE+" §7(§c-1000§8/§a+1000§7)");
				Bukkit.broadcastMessage("  §6Taut de drop pour les flint: §e"+RulesManager.FLINT_DROP_RATE+"§6%");
				Bukkit.broadcastMessage("  §6Taut de drop pour les pommes: §e"+RulesManager.APPLE_DROP_RATE+"§6%");
				Bukkit.broadcastMessage("");
				Bukkit.broadcastMessage("§6Temps:");
				
				Bukkit.broadcastMessage("  §6Dommage: §e" + DateUtils.getTimerDate(2*60));
				Bukkit.broadcastMessage("  §6Bordure: §e" + DateUtils.getTimerDate(GameManager.BORDER_RUNNABLE.getTimer()));
				Bukkit.broadcastMessage("  §6PvP: §e" + DateUtils.getTimerDate(GameManager.PVP_RUNNABLE.getTimer()));
				Bukkit.broadcastMessage("");
				Bukkit.broadcastMessage("§7§M--------------------------------");
				return;
			} 
			if (commandSender instanceof Player) {
				BloostryPlayer iplayer = BloostryAPI.getBloostryPlayer(((Player) commandSender).getUniqueId());
				if (iplayer.getBloostryRank().getPermissionPower() < Ranks.DEVELOPER.getBloostryRank().getPermissionPower()) {
					commandSender.sendMessage(
							GameManager.PREFIX + "§CVous n'avez pas le rang requis pour faire cette commande !");
					return;
				}
			}
			if (arg1.equalsIgnoreCase("generate")) {
				if (args.length == 2) {
					try {
						Integer radius = Integer.valueOf(args[1]);
						GameManager.loadChunks("uhc", radius, 0, 0);
					} catch (Exception e) {
						commandSender.sendMessage(GameManager.PREFIX + "§cVeuillez mettre un nombre entier !");
						return;
					}
				}
			} else if (arg1.equalsIgnoreCase("test")) {
				String arg2 = args[1];
				if (arg2.equalsIgnoreCase("tpworld")) {
					String world = args[2];
					((Player) commandSender).teleport(new Location(Bukkit.getWorld(world),
							((Player) commandSender).getLocation().getX(),
							((Player) commandSender).getLocation().getY(),
							((Player) commandSender).getLocation().getZ()));
					commandSender.sendMessage("§6teleport !");
				} else if (arg2.equalsIgnoreCase("getworld")) {
					commandSender.sendMessage("§6Your world: §e" + ((Player) commandSender).getWorld().getName());
				} else if (arg2.equalsIgnoreCase("playerhead")) {
					String arg3 = args[2];
					if (args.length == 3) {
						if (commandSender instanceof Player) {
							Player player = ((Player) commandSender);
							ItemStack skullVictim = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
							SkullMeta skullM = (SkullMeta) skullVictim.getItemMeta();
							skullM.setDisplayName("§6Tête de §e" + arg3);
							skullM.setOwner(arg3);
							skullVictim.setItemMeta(skullM);
							player.getInventory().addItem(skullVictim);
						} else {
							commandSender.sendMessage("§cVous n'êtes pas un joueur pour éxécuter cette commande :/");
						}
					} else {
						commandSender.sendMessage("§cErreur, essayez: /uhchost test playerhead <joueur>");
					}
				}
			} else if (arg1.equalsIgnoreCase("forcestart")) {
				if (args.length == 1) {
					if (GameManager.gameState.stade == Step.INGAME) {
						commandSender.sendMessage(GameManager.PREFIX + "§cLa partie a déjà commencée ");
						return;
					}
					Bukkit.broadcastMessage(
							"§6Le lancement de la partie a était forcé par §e" + commandSender.getName() + "§6.");
					GameManager.start(commandSender.getName());
				} else {
					commandSender.sendMessage(COMMAND_INVALIDE);
				}
			} else if (arg1.equalsIgnoreCase("start")) {
				if (args.length == 1) {
					if (GameManager.gameState.stade == Step.INGAME) {
						commandSender.sendMessage(GameManager.PREFIX + "§cLa partie a déjà commencée ");
						return;
					}
					if (hasTweeted) {
						GameManager.start(commandSender.getName());
					} else {
						commandSender
								.sendMessage(GameManager.PREFIX + " §cVous n'avez pas envoyé de tweet pour l'UHC !");
					}
				} else {
					commandSender.sendMessage(COMMAND_INVALIDE);
				}
			} else if (arg1.equalsIgnoreCase("tweet")) {
				if (args.length == 2) {
					if (GameManager.gameState.stade == Step.INGAME) {
						commandSender.sendMessage(GameManager.PREFIX + "§cLa partie a déjà commencée ");
						return;
					}
					if (hasTweeted) {
						commandSender.sendMessage(GameManager.PREFIX + "§cVous avez déjà envoyé un tweet");
						return;
					} else {
						hasTweeted = true;
						String scenarios = null;
						for (Scenarios sc : Scenarios.values()) {
							if (sc.iScenario.isEnabled()) {
								if (scenarios == null) {
									scenarios = sc.iScenario.getName();
								} else {
									scenarios += " " + sc.iScenario.getName();
								}
							}
						}
						if (scenarios == null) {
							scenarios = "Aucun scénarios";
						}
						GameManager.HOST_NAME = commandSender.getName();
						try {
							Main.bloostryAPI.getTwitter()
									.updateStatus("🚩 BloostryUHC | #" + GameManager.getHostNumber() + " 🚩\r\n"
											+ "\r\n" + "🔴 http://play.bloostrynetwork.fr \r\n" + "⏰ " + args[1]
											+ "\r\n" + "⛔️ " + GameManager.MAX_PLAYERS + " slots\r\n" + "▶️ "
											+ scenarios + "\r\n" + "👨‍️ Host par " + GameManager.HOST_NAME);
							commandSender.sendMessage(GameManager.PREFIX + "§aLe tweet a était envoyé avec succès.");
							return;
						} catch (TwitterException e) {
							e.printStackTrace();
						}
						commandSender.sendMessage(GameManager.PREFIX + "§cLe tweet n'a pas pu s'envoyer !");
					}
				} else {
					commandSender.sendMessage(COMMAND_INVALIDE);
				}
			} else if (arg1.equalsIgnoreCase("stop")) {
				if (args.length > 1) {
					if (GameManager.gameState.stade != Step.INGAME) {
						commandSender.sendMessage(GameManager.PREFIX + "§cLa partie n'a pas commencée ");
						return;
					}
					String reason = "";
					for (int i = 1; i < args.length; i++) {
						reason += " " + args[i];
					}

					GameManager.stopGame(commandSender.getName(), reason);

					try {
						Main.bloostryAPI.getTwitter()
								.updateStatus("⚠️ L'UHC #" + GameManager.getHostNumber() + " vient d'être annulé par "
										+ commandSender.getName() + " ⚠️\r\n" + "\r\n" + "📌 " + reason);
						commandSender.sendMessage(GameManager.PREFIX + "§aLe tweet a était envoyé avec succès.");
						return;
					} catch (TwitterException e) {
						e.printStackTrace();
					}
					commandSender.sendMessage(GameManager.PREFIX + "§cLe tweet n'a pas pu s'envoyer !");
				} else {
					if (GameManager.gameState == GameState.FINISH) {
						GameManager.stopGame(commandSender.getName(), "Fin de la partie");
						return;
					}
					commandSender.sendMessage(COMMAND_INVALIDE);
				}
			} else if (arg1.equalsIgnoreCase("restart")) {
				if (args.length == 1) {
					GameManager.restart();
				} else {
					commandSender.sendMessage(COMMAND_INVALIDE);
				}
			} else if (arg1.equalsIgnoreCase("open")) {
				if (args.length == 2) {
					String arg2 = args[1];
					GameManager.open(arg2);
				} else {
					commandSender.sendMessage(COMMAND_INVALIDE);
				}
			} else if (arg1.equalsIgnoreCase("config")) {
				if (args.length == 1) {
					if (commandSender instanceof Player) {
						GameManager.config((Player) commandSender);
					}
				} else {
					commandSender.sendMessage(COMMAND_INVALIDE);
				}
			} else if (arg1.equalsIgnoreCase(
					"pre-whitelist")) {/*
										 * if (args.length == 4) { String arg2 = args[1]; // add / remove String arg3 =
										 * args[2]; // player <antox11200> String arg4 = args[3]; // ex = 10 Integer
										 * numberWhitelist = 0; try { numberWhitelist = Integer.valueOf(arg4); } catch
										 * (Exception e) { commandSender.sendMessage(PLEASE_SET_NUMBER); return; } if
										 * (commandSender instanceof Player) { IPlayer iplayer =
										 * BPlayer.getBPlayer(arg3); if(iplayer == null) {
										 * commandSender.sendMessage(GameManager.PREFIX +
										 * "§cCe joueur n'est pas enregistré dans la base de donnée !"); return; } if
										 * (!(arg2.equalsIgnoreCase("remove") && arg2.equalsIgnoreCase("add"))) {
										 * commandSender.sendMessage(COMMAND_INVALIDE);
										 * Console.sendMessageDebug("not remove or add"); } else {
										 * if(GameManager.preWhitelist(arg2, iplayer, numberWhitelist)) {
										 * if(arg2.equalsIgnoreCase("remove")) {
										 * commandSender.sendMessage("§6Vous avez retiré §c"
										 * +numberWhitelist+" §6pré-whitelist au joueur §e"+iplayer.getName()+"§6."); }
										 * else if (arg2.equalsIgnoreCase("add")) {
										 * commandSender.sendMessage("§6Vous avez ajouté §a"
										 * +numberWhitelist+" §6pré-whitelist au joueur §e"+iplayer.getName()+"§6."); }
										 * } else { commandSender.sendMessage(COMMAND_INVALIDE);
										 * Console.sendMessageDebug("return false"); } } } } else {
										 * commandSender.sendMessage(COMMAND_INVALIDE); }
										 */
			} else {
				commandSender.sendMessage(COMMAND_INVALIDE);
			}
		}
	}

	@Override
	public TabCompleter getTabCompleter() {
		return new TabCompleter() {
			@Override
			public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
				ArrayList<String> list = new ArrayList<>();
				if (args.length <= 1) {
					list.add("open");
					list.add("start");
					list.add("tweet");
					list.add("stop");
					list.add("restart");
					list.add("pre-whitelist");
					list.add("config");
					return list;
				} else if (args.length == 3) {
					if (args[1].equalsIgnoreCase("open")) {
						list.add("whitelist");
						list.add("all");
						return list;
					}
					if (args[1].equalsIgnoreCase("pre-whitelist")) {
						list.add("add");
						list.add("remove");
						return list;
					}
				}
				return null;
			}
		};
	}
}
