package mc.bloostrynetwork.antox11200.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import mc.bloostrynetwork.antox11200.bloostryapi.BloostryAPI;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.commands.ICommand;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.player.BloostryPlayer;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.rank.Ranks;
import mc.bloostrynetwork.antox11200.manager.game.GameManager;
import mc.bloostrynetwork.antox11200.player.UPlayer;

public class ReviveCommand implements ICommand {

	@Override
	public String getCommand() {
		return "revive";
	}

	@Override
	public TabCompleter getTabCompleter() {
		return null;
	}

	@Override
	public void handle(CommandSender commandSender, String[] args) {
		if(args.length == 1) {
			String target = args[0];
			if(commandSender instanceof Player) {
				BloostryPlayer bloostryPlayer = BloostryAPI.getBloostryPlayer(((Player) commandSender).getUniqueId());
				if(bloostryPlayer.getBloostryRank().getPermissionPower() < Ranks.HOST.getBloostryRank().getPermissionPower()) {
					commandSender.sendMessage(GameManager.PREFIX+"�cVous n'avez pas le rang requis pour faire cette commande !");
					return;
				}
			}
			if(GameManager.players.containsKey(target)) {
				UPlayer uplayer = GameManager.players.get(target);
				if(uplayer.dead) {
					uplayer.revive();
					commandSender.sendMessage(GameManager.PREFIX+"�6Vous avez ramen�e �e"+target+" �6� la vie :D");
				} else {
					commandSender.sendMessage(GameManager.PREFIX+"�cCe joueur n'est pas mort !");
				}
			} else {
				commandSender.sendMessage(GameManager.PREFIX+"�cCe joueur ne s'est jamais connecter pour jouer � la partie");
			}
		} else {
			commandSender.sendMessage(GameManager.PREFIX+"�cErreur,essayez: �e/revive <joueur>");
		}
	}
}
