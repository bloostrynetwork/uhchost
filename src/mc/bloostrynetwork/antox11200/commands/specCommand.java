package mc.bloostrynetwork.antox11200.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import mc.bloostrynetwork.antox11200.bloostryapi.BloostryAPI;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.commands.ICommand;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.player.BloostryPlayer;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.rank.Ranks;
import mc.bloostrynetwork.antox11200.manager.game.GameManager;
import mc.bloostrynetwork.antox11200.player.UPlayer;

public class specCommand implements ICommand {

	@Override
	public String getCommand() {
		return "spectator";
	}

	@Override
	public TabCompleter getTabCompleter() {
		return null;
	}

	@Override
	public void handle(CommandSender commandSender, String[] args) {
		if (commandSender instanceof Player) {
			BloostryPlayer bloostryPlayer = BloostryAPI.getBloostryPlayer(((Player) commandSender).getUniqueId());
			if (bloostryPlayer != null) {
				if (bloostryPlayer.getBloostryRank().getPermissionPower() < Ranks.HOST.getBloostryRank()
						.getPermissionPower()) {
					commandSender.sendMessage(
							GameManager.PREFIX + "�cVous n'avez pas le rang requis pour �x�cuter cette commande !");
					return;
				}
			} else {
				commandSender.sendMessage(GameManager.PREFIX + "�cVous n'�tes dans la base de donn�e !");
				return;
			}
		}
		if (args.length == 0) {
			if (commandSender instanceof Player) {
				UPlayer uplayer = GameManager.players.get(commandSender.getName());
				if (uplayer != null) {
					uplayer.specMode = true;
				} else {
					commandSender.sendMessage(GameManager.PREFIX + "�cLe joueur n'est pas r�cup�rable !");
				}
			} else {
				commandSender.sendMessage(GameManager.PREFIX + "�cVous n'�tes pas un joueur !");
			}
		} else if (args.length == 1) {
			String target = args[0];
			Player targetPlayer = Bukkit.getPlayer(target);
			if (targetPlayer != null) {
				UPlayer uplayer = GameManager.players.get(target);
				if (uplayer != null) {
					uplayer.specMode = true;
				} else {
					commandSender.sendMessage(GameManager.PREFIX + "�cLe joueur n'est pas r�cup�rable !");
				}
			} else {
				commandSender.sendMessage(GameManager.PREFIX + "�cCe joueur n'est pas en ligne !");
			}
		} else {

		}
	}
}
