package mc.bloostrynetwork.antox11200.utils;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.World;

public class ChunkLocation {

	private World world;
	
	private int x;
	private int z;
	
	private Chunk chunk;
	
	public ChunkLocation(String world, int x, int z) {
		this.world = Bukkit.getWorld(world);
		this.x = x;
		this.z = z;
		chunk = this.world.getChunkAt(x >> 4, z >> 4);
	}
	
	public int getX() {
		return this.x;
	}
	
	public int getZ() {
		return this.z;
	}
	
	public World getWorld() {
		return this.world;
	}
	
	public Chunk getChunk() {
		return this.chunk;
	}
}
