package mc.bloostrynetwork.antox11200.utils;

import org.bukkit.Location;
import org.bukkit.entity.Player;

public class DirectionUtils {

	public static double getAngle(Player p, Location Loc) {
		Location Locp = p.getLocation();
		if (Locp.getWorld() == Loc.getWorld()) {
			if (!(Locp.getBlockX() == Loc.getBlockX() && Locp.getBlockZ() == Loc.getBlockZ())) {
				// Angle en degré = 180 * (angle en radian) / pi
				double xp = Locp.getBlockX();
				double zp = Locp.getBlockZ();

				double xl = Loc.getBlockX();
				double zl = Loc.getBlockZ();

				double distancex = xp - xl;
				double distancecx;
				if (distancex < 0) {
					distancecx = -distancex;
				} else {
					distancecx = distancex;
				}

				double distancez = zp - zl;
				double distancecz;
				if (distancez < 0) {
					distancecz = -distancez;
				} else {
					distancecz = distancez;
				}

				double angle = ((180 * (Math.atan(distancecz / distancecx))) / Math.PI);

				if (distancex >= 0 && distancez >= 0) {
				} else if (distancex < 0 && distancez >= 0) {
					angle = (90 - angle) + 90;
				} else if (distancex <= 0 && distancez < 0) {
					angle = angle + 180;
				} else if (distancex > 0 && distancez < 0) {
					angle = (90 - angle) + 270;
				}

				angle = angle + 270;
				if (angle >= 360)
					angle = angle - 360;

				double yaw = ((p.getEyeLocation().getYaw()) + 180);

				angle = angle - yaw;

				if (angle <= 0)
					angle = angle + 360;
				if (angle <= 0)
					angle = angle + 360;

				return angle;
			} else {
				return -1;
			}
		}
		return -2;
	}

	public static String getArrow(double angle) {
		String c = "";
        if (angle == -2) {
            c = "";
        } else if (angle == -1) {
            c = "X";
        } else if ((angle < 22.5 && angle >= 0) || angle > 337.5) {
            c = "⬆";
        } else if (angle < 67.5 && angle > 22.5) {
            c = "⬈";
        } else if (angle < 112.5 && angle > 67.5) {
            c = "➡";
        } else if (angle < 157.5 && angle > 112.5) {
            c = "⬊";
        } else if (angle < 202.5 && angle > 157.5) {
            c = "⬇";
        } else if (angle < 247.5 && angle > 202.5) {
            c = "⬋";
        } else if (angle < 292.5 && angle > 247.5) {
            c = "⬅";
        } else if (angle < 337.5 && angle > 292.5) {
            c = "⬉";
        }
		return c;
	}
}
