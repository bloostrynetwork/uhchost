package mc.bloostrynetwork.antox11200.inventorys.config;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;

import mc.bloostrynetwork.antox11200.bloostryapi.utils.DateUtils;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.console.Console;
import mc.bloostrynetwork.antox11200.inventorys.GuiMain;
import mc.bloostrynetwork.antox11200.inventorys.IGui;
import mc.bloostrynetwork.antox11200.inventorys.Inventory;
import mc.bloostrynetwork.antox11200.items.Item;
import mc.bloostrynetwork.antox11200.items.ItemHandler;
import mc.bloostrynetwork.antox11200.manager.game.GameManager;
import mc.bloostrynetwork.antox11200.manager.game.team.TeamType;
import mc.bloostrynetwork.antox11200.player.UPlayer;

public class GuiPropertiesGame implements IGui {

	public static final Inventory GUI = new Inventory(null, "�6Propri�t� de la partie",
			InventoryType.CHEST.getDefaultSize() * 2);

	public static final HashMap<Integer, Item> items = new HashMap<>();

	public void refresh(Player player) {
		Item borderUpTenMinutes = new Item(Material.BANNER, "�a+10 minutes", null, DyeColor.GREEN, new ItemHandler() {
			@Override
			public void handle(Player player) {
				int timer = GameManager.BORDER_RUNNABLE.timer + 60 * 10;
				if (timer > GameManager.BORDER_MAX_TIME)
					return;
				GameManager.BORDER_RUNNABLE.timer = timer;
			}
		});

		Item borderUpFiveMinutes = new Item(Material.BANNER, "�a+5 minutes", null, DyeColor.GREEN, new ItemHandler() {
			@Override
			public void handle(Player player) {
				int timer = GameManager.BORDER_RUNNABLE.timer + 60 * 5;
				if (timer > GameManager.BORDER_MAX_TIME)
					return;
				GameManager.BORDER_RUNNABLE.timer = timer;
			}
		});

		Item borderUpOneMinutes = new Item(Material.BANNER, "�a+1 minutes", null, DyeColor.GREEN, new ItemHandler() {
			@Override
			public void handle(Player player) {
				int timer = GameManager.BORDER_RUNNABLE.timer + 60;
				if (timer > GameManager.BORDER_MAX_TIME)
					return;
				GameManager.BORDER_RUNNABLE.timer = timer;
			}
		});

		Item borderDownTenMinutes = new Item(Material.BANNER, "�c-10 minutes", null, DyeColor.RED, new ItemHandler() {
			@Override
			public void handle(Player player) {
				int timer = GameManager.BORDER_RUNNABLE.timer - 60 * 10;
				if (timer < 60)
					return;
				GameManager.BORDER_RUNNABLE.timer = timer;
			}
		});

		Item borderDownFiveMinutes = new Item(Material.BANNER, "�c-5 minutes", null, DyeColor.RED, new ItemHandler() {
			@Override
			public void handle(Player player) {
				int timer = GameManager.BORDER_RUNNABLE.timer - 60 * 5;
				if (timer < 60)
					return;
				GameManager.BORDER_RUNNABLE.timer = timer;
			}
		});

		Item borderDownOneMinutes = new Item(Material.BANNER, "�c-1 minutes", null, DyeColor.RED, new ItemHandler() {
			@Override
			public void handle(Player player) {
				int timer = GameManager.BORDER_RUNNABLE.timer - 60;
				if (timer < 60)
					return;
				GameManager.BORDER_RUNNABLE.timer = timer;
			}
		});

		Item pvpUpTenMinutes = new Item(Material.BANNER, "�a+10 minutes", null, DyeColor.GREEN, new ItemHandler() {
			@Override
			public void handle(Player player) {
				int timer = GameManager.PVP_RUNNABLE.timer + 60 * 10;
				if (timer > GameManager.PVP_MAX_TIME)
					return;
				GameManager.PVP_RUNNABLE.timer = timer;
			}
		});

		Item pvpUpFiveMinutes = new Item(Material.BANNER, "�a+5 minutes", null, DyeColor.GREEN, new ItemHandler() {
			@Override
			public void handle(Player player) {
				int timer = GameManager.PVP_RUNNABLE.timer + 60 * 5;
				if (timer > GameManager.PVP_MAX_TIME)
					return;
				GameManager.PVP_RUNNABLE.timer = timer;
			}
		});

		Item pvpUpOneMinutes = new Item(Material.BANNER, "�a+1 minutes", null, DyeColor.GREEN, new ItemHandler() {
			@Override
			public void handle(Player player) {
				int timer = GameManager.PVP_RUNNABLE.timer + 60;
				if (timer > GameManager.PVP_MAX_TIME)
					return;
				GameManager.PVP_RUNNABLE.timer = timer;
			}
		});

		Item pvpDownTenMinutes = new Item(Material.BANNER, "�c-10 minutes", null, DyeColor.RED, new ItemHandler() {
			@Override
			public void handle(Player player) {
				int timer = GameManager.PVP_RUNNABLE.timer - 60 * 10;
				if (timer < 60)
					return;
				GameManager.PVP_RUNNABLE.timer = timer;
			}
		});

		Item pvpDownFiveMinutes = new Item(Material.BANNER, "�c-5 minutes", null, DyeColor.RED, new ItemHandler() {
			@Override
			public void handle(Player player) {
				int timer = GameManager.PVP_RUNNABLE.timer - 60 * 5;
				if (timer < 60)
					return;
				GameManager.PVP_RUNNABLE.timer = timer;
			}
		});

		Item pvpDownOneMinutes = new Item(Material.BANNER, "�c-1 minutes", null, DyeColor.RED, new ItemHandler() {
			@Override
			public void handle(Player player) {
				int timer = GameManager.PVP_RUNNABLE.timer - 60;
				if (timer < 60)
					return;
				GameManager.PVP_RUNNABLE.timer = timer;
			}
		});

		Item borderSizeUp500Block = new Item(Material.BANNER, "�a+500 blocs", null, DyeColor.GREEN, new ItemHandler() {
			@Override
			public void handle(Player player) {
				int border = GameManager.BORDER_SIZE + 500;
				if (border > GameManager.BORDER_MAX_SIZE || border < GameManager.BORDER_MIN_SIZE)
					return;
				GameManager.BORDER_SIZE = border;
			}
		});

		Item borderSizeUp100Block = new Item(Material.BANNER, "�a+100 blocs", null, DyeColor.GREEN, new ItemHandler() {
			@Override
			public void handle(Player player) {
				int border = GameManager.BORDER_SIZE + 100;
				if (border > GameManager.BORDER_MAX_SIZE || border < GameManager.BORDER_MIN_SIZE)
					return;
				GameManager.BORDER_SIZE = border;
			}
		});

		Item borderSizeUp50Block = new Item(Material.BANNER, "�a+50 blocs", null, DyeColor.GREEN, new ItemHandler() {
			@Override
			public void handle(Player player) {
				int border = GameManager.BORDER_SIZE + 50;
				if (border > GameManager.BORDER_MAX_SIZE || border < GameManager.BORDER_MIN_SIZE)
					return;
				GameManager.BORDER_SIZE = border;
			}
		});

		Item borderSizeDown500Block = new Item(Material.BANNER, "�c-500 blocs", null, DyeColor.RED, new ItemHandler() {
			@Override
			public void handle(Player player) {
				int border = GameManager.BORDER_SIZE - 500;
				if (border > GameManager.BORDER_MAX_SIZE || border < GameManager.BORDER_MIN_SIZE)
					return;
				GameManager.BORDER_SIZE = border;
			}
		});

		Item borderSizeDown100Block = new Item(Material.BANNER, "�c-100 blocs", null, DyeColor.RED, new ItemHandler() {
			@Override
			public void handle(Player player) {
				int border = GameManager.BORDER_SIZE - 100;
				if (border > GameManager.BORDER_MAX_SIZE || border < GameManager.BORDER_MIN_SIZE)
					return;
				GameManager.BORDER_SIZE = border;
			}
		});

		Item borderSizeDown50Block = new Item(Material.BANNER, "�c-50 blocs", null, DyeColor.RED, new ItemHandler() {
			@Override
			public void handle(Player player) {
				int border = GameManager.BORDER_SIZE - 50;
				if (border > GameManager.BORDER_MAX_SIZE || border < GameManager.BORDER_MIN_SIZE)
					return;
				GameManager.BORDER_SIZE = border;
			}
		});

		Item slotSizeUp5 = new Item(Material.BANNER, "�a+5 slots", null, DyeColor.GREEN, new ItemHandler() {
			@Override
			public void handle(Player player) {
				int slot = GameManager.MAX_PLAYERS + 5;
				if (slot > GameManager.VAR_MAX_PLAYERS)
					return;
				GameManager.MAX_PLAYERS = slot;
				Console.sendMessageDebug("init variable: " + GameManager.MAX_PLAYERS + " / " + slot);
			}
		});

		Item slotSizeUp10 = new Item(Material.BANNER, "�a+10 slots", null, DyeColor.GREEN, new ItemHandler() {
			@Override
			public void handle(Player player) {
				int slot = GameManager.MAX_PLAYERS + 10;
				if (slot > GameManager.VAR_MAX_PLAYERS)
					return;
				GameManager.MAX_PLAYERS = slot;
			}
		});

		Item slotSizeUp20 = new Item(Material.BANNER, "�a+20 slots", null, DyeColor.GREEN, new ItemHandler() {
			@Override
			public void handle(Player player) {
				int slot = GameManager.MAX_PLAYERS + 20;
				if (slot > GameManager.VAR_MAX_PLAYERS)
					return;
				GameManager.MAX_PLAYERS = slot;
			}
		});

		Item slotSizeDown5 = new Item(Material.BANNER, "�c-5 slots", null, DyeColor.RED, new ItemHandler() {
			@Override
			public void handle(Player player) {
				int slot = GameManager.MAX_PLAYERS - 5;
				if (slot < 0)//60 < 80
					return;
				GameManager.MAX_PLAYERS = slot;
			}
		});

		Item slotSizeDown10 = new Item(Material.BANNER, "�c-10 slots", null, DyeColor.RED, new ItemHandler() {
			@Override
			public void handle(Player player) {
				int slot = GameManager.MAX_PLAYERS - 10;
				if (slot < 0)//60 < 80
					return;
				GameManager.MAX_PLAYERS = slot;
			}
		});

		Item slotSizeDown20 = new Item(Material.BANNER, "�c-20 slots", null, DyeColor.RED, new ItemHandler() {
			@Override
			public void handle(Player player) {
				int slot = GameManager.MAX_PLAYERS - 20;
				if (slot < 0)//60 < 80
					return;
				GameManager.MAX_PLAYERS = slot;
			}
		});

		// ICON

		Item borderSize = new Item(Material.BEACON,
				"�6Taille de la bordure �8(�6" + GameManager.BORDER_SIZE + "�7x�6" + GameManager.BORDER_SIZE + "�8)",
				null, null);

		Item borderTimeConfig = new Item(Material.PAPER,
				"�6Temps de la bordure �8(�6" + DateUtils.getTimerDate(GameManager.BORDER_RUNNABLE.timer) + "�8)",
				null, null);

		Item slotIcon = new Item(Material.SLIME_BALL,
				"�6Nombre de slot �8(�6" + GameManager.MAX_PLAYERS + " slots�8)",
				null, null);

		Item pvpTimeConfig = new Item(Material.DIAMOND_SWORD,
				"�6Temps du PvP �8(�6" + DateUtils.getTimerDate(GameManager.PVP_RUNNABLE.timer) + "�8)", null, null);

		Item backToMain = new Item(Material.PAPER, "�cRetour au menu principal", null, new ItemHandler() {
			@Override
			public void handle(Player player) {
				player.closeInventory();
				new GuiMain().open(player);
			}
		});

		items.put(1, borderUpOneMinutes);
		items.put(2, borderUpFiveMinutes);
		items.put(3, borderUpTenMinutes);

		items.put(1 + 6, borderDownTenMinutes);
		items.put(2 + 6, borderDownFiveMinutes);
		items.put(3 + 6, borderDownOneMinutes);

		items.put(9 + 1, pvpUpOneMinutes);
		items.put(9 + 2, pvpUpFiveMinutes);
		items.put(9 + 3, pvpUpTenMinutes);

		items.put(9 + 1 + 6, pvpDownTenMinutes);
		items.put(9 + 2 + 6, pvpDownFiveMinutes);
		items.put(9 + 3 + 6, pvpDownOneMinutes);

		items.put(9 * 2 + 1, borderSizeUp50Block);
		items.put(9 * 2 + 2, borderSizeUp100Block);
		items.put(9 * 2 + 3, borderSizeUp500Block);

		items.put(9 * 2 + 1 + 6, borderSizeDown500Block);
		items.put(9 * 2 + 2 + 6, borderSizeDown100Block);
		items.put(9 * 2 + 3 + 6, borderSizeDown50Block);

		items.put(9 * 3 + 1, slotSizeUp5);
		items.put(9 * 3 + 2, slotSizeUp10);
		items.put(9 * 3 + 3, slotSizeUp20);

		items.put(9 * 3 + 3 + 6, slotSizeDown5);
		items.put(9 * 3 + 2 + 6, slotSizeDown10);
		items.put(9 * 3 + 1 + 6, slotSizeDown20);

		int i = 9 * 5 + 3;

		for (TeamType teamTypes : TeamType.values()) {
			Item itemTeam = new Item(Material.PAPER, "�6" + teamTypes.name, null, new ItemHandler() {
				@Override
				public void handle(Player player) {
					boolean hasTeam = GameManager.hasTeam();
					GameManager.teamType = teamTypes;
					if (hasTeam && GameManager.hasTeam())
						return;
					if (GameManager.teamType == TeamType.FFA) {
						for (UPlayer uplayers : GameManager.players.values()) {
							if (uplayers.team != null) {
								if (uplayers.team.getOwner().getName().equals(uplayers.getName())) {
									uplayers.team.delete();
								}
							}
						}
					}
					Bukkit.broadcastMessage(GameManager.PREFIX + "�6Les �quipes sont d�sormais "
							+ (teamTypes == TeamType.FFA ? "�cd�sactiv�e" : "�aactiv�e") + "�6.");
				}
			});
			items.put(i, itemTeam);
			i++;
		}

		items.put(5, borderTimeConfig);
		items.put(9 + 5, pvpTimeConfig);
		items.put(9 * 2 + 5, borderSize);
		items.put(9 * 3 + 5, slotIcon);

		items.put(27 * 2, backToMain);

		org.bukkit.inventory.InventoryView invOpen = player.getOpenInventory();

		for (Integer slot : items.keySet()) {
			slot--;
			invOpen.setItem(slot, items.get(slot + 1).toItemStack());
		}
	}

	public Inventory getGUI() {
		return GUI;
	}

	@Override
	public void open(Player player) {
		player.closeInventory();
		player.openInventory(GUI.toInventory());
		if (player.getOpenInventory().getTitle().equals(GUI.getTitle())) {
			refresh(player);
		}
	}

	@Override
	public void clickOnItem(Player player, int slot) {
		for (Integer itemsSlot : items.keySet()) {
			Item item = items.get(itemsSlot);
			if (slot == itemsSlot - 1) {
				item.handle(player);
				if (player.getOpenInventory().getTitle().equals(GUI.getTitle())) {
					refresh(player);
				}
			}
		}
	}
}
