package mc.bloostrynetwork.antox11200.inventorys.scenarios;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;

import mc.bloostrynetwork.antox11200.inventorys.GuiMain;
import mc.bloostrynetwork.antox11200.inventorys.IGui;
import mc.bloostrynetwork.antox11200.inventorys.Inventory;
import mc.bloostrynetwork.antox11200.items.Item;
import mc.bloostrynetwork.antox11200.items.ItemHandler;
import mc.bloostrynetwork.antox11200.manager.game.scenarios.depends.Scenarios;

public class GuiEditScenarios implements IGui {

	public static final Inventory GUI = new Inventory(null, "�6Configuration des sc�narios", InventoryType.CHEST);

	public static final HashMap<Integer, Item> items = new HashMap<>();

	public void refresh(Player player) {
		int i = 1;
		for (Scenarios scenarios : Scenarios.values()) {
			List<String> lore = new ArrayList<>();
			String[] desc = scenarios.iScenario.getDescription().split(",");
			for(String messages : desc){
				lore.add(messages);
			}
			if (!(i >= 26)) {
				
				Item scenariosItem = new Item(scenarios.iScenario.getIcon().getType(), scenarios.iScenario.getIcon().getAmount(),
						"�6" + scenarios.iScenario.getName() + " �8("
								+ (scenarios.iScenario.isEnabled() ? "�aActiv�" : "�cD�sactiv�") + "�8)",
						lore, new ItemHandler() {
							@Override
							public void handle(Player player) {
								scenarios.iScenario.setEnabled(!scenarios.iScenario.isEnabled());
								/*player.sendMessage(GameManager.PREFIX + "�6Le sc�nario �e"
										+ scenarios.iScenario.getName() + " �6vient d'�tre "
										+ (scenarios.iScenario.isEnabled() ? "�aActiv�" : "�cD�sactiv�") + "�6.");*/
							}
						});
				items.put(i, scenariosItem);
				i++;
			} else {
				break;
			}
		}
		
		
		Item backToMain = new Item(Material.PAPER, "�cRetour au menu principal",
				null, new ItemHandler() {
					@Override
					public void handle(Player player) {
						player.closeInventory();
						new GuiMain().open(player);
					}
				});

		items.put(27, backToMain);

		org.bukkit.inventory.InventoryView invOpen = player.getOpenInventory();

		for (Integer slot : items.keySet()) {
			slot--;
			invOpen.setItem(slot, items.get(slot + 1).toItemStack());
		}
	}

	public Inventory getGUI() {
		return GUI;
	}

	@Override
	public void open(Player player) {
		player.closeInventory();
		player.openInventory(GUI.toInventory());
		if(player.getOpenInventory().getTitle().equals(GUI.getTitle())) {
			refresh(player);	
		}
	}

	@Override
	public void clickOnItem(Player player, int slot) {
		for (Integer itemsSlot : items.keySet()) {
			Item item = items.get(itemsSlot);
			if (slot == itemsSlot - 1) {
				item.handle(player);
				if(player.getOpenInventory().getTitle().equals(GUI.getTitle())) {
					refresh(player);	
				}
			}
		}
	}
}
