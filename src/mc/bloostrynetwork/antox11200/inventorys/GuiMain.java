package mc.bloostrynetwork.antox11200.inventorys;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;

import mc.bloostrynetwork.antox11200.inventorys.config.GuiPropertiesGame;
import mc.bloostrynetwork.antox11200.inventorys.rules.GuiRulesConfigueration;
import mc.bloostrynetwork.antox11200.inventorys.scenarios.GuiEditScenarios;
import mc.bloostrynetwork.antox11200.items.Item;
import mc.bloostrynetwork.antox11200.items.ItemHandler;
import mc.bloostrynetwork.antox11200.manager.game.GameManager;
import mc.bloostrynetwork.antox11200.manager.game.scenarios.depends.Scenarios;

public class GuiMain implements IGui {

	public static final Inventory GUI = new Inventory(null, "�6Configuration de la partie", InventoryType.CHEST);

	public static final HashMap<Integer, Item> items = new HashMap<>();
	
	public void refresh(Player player){
		ArrayList<String> lore = new ArrayList<>();
		lore.add("�7�m----------------------");
		lore.add("");
		for(Scenarios scenarios : Scenarios.values()) {
			if(scenarios.iScenario.isEnabled()) {
				lore.add("�7- �6"+scenarios.iScenario.getName());	
			}
		}
		lore.add("");
		lore.add("�7�m----------------------");
		Item scenarios = new Item(Material.BOOK, "�6S�lectionner les sc�narios", lore, new ItemHandler() {
			@Override
			public void handle(Player player) {
				new GuiEditScenarios().open(player);
			}
		});
		Item rules = new Item(Material.BOOK_AND_QUILL, "�6D�finir les r�gles", null, new ItemHandler() {
			@Override
			public void handle(Player player) {
				new GuiRulesConfigueration().open(player);
			}
		});
		Item config = new Item(Material.COMMAND, "�6Propri�t�s de la partie", null, new ItemHandler() {
			@Override
			public void handle(Player player) {
				new GuiPropertiesGame().open(player);
			}
		});

		items.put(9 + 2, scenarios);
		items.put(9 + 5, rules);
		items.put(9 + 8, config);
		
		org.bukkit.inventory.InventoryView invOpen = player.getOpenInventory();


		for(Integer slot : IGui.getDefaultItem().keySet()) {
			invOpen.setItem(slot, IGui.getDefaultItem().get(slot));
		}
		
		for(Integer slot : items.keySet()) {
			slot--;
			invOpen.setItem(slot, items.get(slot + 1).toItemStack());
		}
	}
	
	public Inventory getGUI() {
		return GUI;
	}
	
	@Override
	public void open(Player player) {
		player.closeInventory();
		player.openInventory(GUI.toInventory());
		if(player.getOpenInventory().getTitle().equals(GUI.getTitle())) {
			refresh(player);	
		}
		player.sendMessage(GameManager.PREFIX + "�6Ouverture du menu de configuraton.");
	}

	@Override
	public void clickOnItem(Player player, int slot) {
		for (Integer itemsSlot : items.keySet()) {
			Item item = items.get(itemsSlot);
			if (slot == itemsSlot - 1) {
				item.handle(player);
				if(player.getOpenInventory().getTitle().equals(GUI.getTitle())) {
					refresh(player);	
				}
			}
		}
	}
}
