package mc.bloostrynetwork.antox11200.inventorys.rules;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;

import mc.bloostrynetwork.antox11200.inventorys.GuiMain;
import mc.bloostrynetwork.antox11200.inventorys.IGui;
import mc.bloostrynetwork.antox11200.inventorys.Inventory;
import mc.bloostrynetwork.antox11200.items.Item;
import mc.bloostrynetwork.antox11200.items.ItemHandler;
import mc.bloostrynetwork.antox11200.manager.game.GameManager;
import mc.bloostrynetwork.antox11200.manager.game.rules.RulesManager;

public class GuiRulesConfigueration implements IGui {

	public static final Inventory GUI = new Inventory(null, "�6Configuration des r�gles", InventoryType.CHEST);

	public static final HashMap<Integer, Item> items = new HashMap<>();

	public void refresh(Player player) {
		Item appleDropRate = new Item(Material.APPLE, "�6Pommes: �e"+RulesManager.APPLE_DROP_RATE+"%", null, null);
		Item flintDropRate = new Item(Material.FLINT, "�6Flint: �e"+RulesManager.FLINT_DROP_RATE+"%", null, null);
		
		Item appleDropRateUp10 = new Item(Material.BANNER, "�a+10%", null, DyeColor.GREEN, new ItemHandler() {
			@Override
			public void handle(Player player) {
				int pourcentage = RulesManager.APPLE_DROP_RATE + 10;
				if(pourcentage > 100 || pourcentage < 0) return;
				RulesManager.APPLE_DROP_RATE = pourcentage;
			}
		});

		Item appleDropRateUp5 = new Item(Material.BANNER, "�a+5%", null, DyeColor.GREEN, new ItemHandler() {
			@Override
			public void handle(Player player) {
				int pourcentage = RulesManager.APPLE_DROP_RATE + 5;
				if(pourcentage > 100 || pourcentage < 0) return;
				RulesManager.APPLE_DROP_RATE = pourcentage;
			}
		});

		Item appleDropRateUp1 = new Item(Material.BANNER, "�a+1%", null, DyeColor.GREEN, new ItemHandler() {
			@Override
			public void handle(Player player) {
				int pourcentage = RulesManager.APPLE_DROP_RATE + 1;
				if(pourcentage > 100 || pourcentage < 0) return;
				RulesManager.APPLE_DROP_RATE = pourcentage;
			}
		});

		Item appleDropRateDown10 = new Item(Material.BANNER, "�c-10%", null, DyeColor.RED, new ItemHandler() {
			@Override
			public void handle(Player player) {
				int pourcentage = RulesManager.APPLE_DROP_RATE - 10;
				if(pourcentage > 100 || pourcentage < 0) return;
				RulesManager.APPLE_DROP_RATE = pourcentage;
			}
		});

		Item appleDropRateDown5 = new Item(Material.BANNER, "�c-5%", null, DyeColor.RED, new ItemHandler() {
			@Override
			public void handle(Player player) {
				int pourcentage = RulesManager.APPLE_DROP_RATE - 5;
				if(pourcentage > 100 || pourcentage < 0) return;
				RulesManager.APPLE_DROP_RATE = pourcentage;
			}
		});

		Item appleDropRateDown1 = new Item(Material.BANNER, "�c-1%", null, DyeColor.RED, new ItemHandler() {
			@Override
			public void handle(Player player) {
				int pourcentage = RulesManager.APPLE_DROP_RATE - 1;
				if(pourcentage > 100 || pourcentage < 0) return;
				RulesManager.APPLE_DROP_RATE = pourcentage;
			}
		});
		
		Item flintDropRateUp10 = new Item(Material.BANNER, "�a+10%", null, DyeColor.GREEN, new ItemHandler() {
			@Override
			public void handle(Player player) {
				int pourcentage = RulesManager.FLINT_DROP_RATE + 10;
				if(pourcentage > 100 || pourcentage < 0) return;
				RulesManager.FLINT_DROP_RATE = pourcentage;
			}
		});

		Item flintDropRateUp5 = new Item(Material.BANNER, "�a+5%", null, DyeColor.GREEN, new ItemHandler() {
			@Override
			public void handle(Player player) {
				int pourcentage = RulesManager.FLINT_DROP_RATE + 5;
				if(pourcentage > 100 || pourcentage < 0) return;
				RulesManager.FLINT_DROP_RATE = pourcentage;
			}
		});

		Item flintDropRateUp1 = new Item(Material.BANNER, "�a+1%", null, DyeColor.GREEN, new ItemHandler() {
			@Override
			public void handle(Player player) {
				int pourcentage = RulesManager.FLINT_DROP_RATE + 1;
				if(pourcentage > 100 || pourcentage < 0) return;
				RulesManager.FLINT_DROP_RATE = pourcentage;
			}
		});

		Item flintDropRateDown10 = new Item(Material.BANNER, "�c-10%", null, DyeColor.RED, new ItemHandler() {
			@Override
			public void handle(Player player) {
				int pourcentage = RulesManager.FLINT_DROP_RATE - 10;
				if(pourcentage > 100 || pourcentage < 0) return;
				RulesManager.FLINT_DROP_RATE = pourcentage;
			}
		});

		Item flintDropRateDown5 = new Item(Material.BANNER, "�c-5%", null, DyeColor.RED, new ItemHandler() {
			@Override
			public void handle(Player player) {
				int pourcentage = RulesManager.FLINT_DROP_RATE - 5;
				if(pourcentage > 100 || pourcentage < 0) return;
				RulesManager.FLINT_DROP_RATE = pourcentage;
			}
		});

		Item flintDropRateDown1 = new Item(Material.BANNER, "�c-1%", null, DyeColor.RED, new ItemHandler() {
			@Override
			public void handle(Player player) {
				int pourcentage = RulesManager.FLINT_DROP_RATE - 1;
				if(pourcentage > 100 || pourcentage < 0) return;
				RulesManager.FLINT_DROP_RATE = pourcentage;
			}
		});

		items.put(1, appleDropRateUp1);
		items.put(2, appleDropRateUp5);
		items.put(3, appleDropRateUp10);

		items.put(1 + 6, appleDropRateDown1);
		items.put(2 + 6, appleDropRateDown5);
		items.put(3 + 6, appleDropRateDown10);

		items.put(9 + 1, flintDropRateUp1);
		items.put(9 + 2, flintDropRateUp5);
		items.put(9 + 3, flintDropRateUp10);

		items.put(9 + 1 + 6, flintDropRateDown1);
		items.put(9 + 2 + 6, flintDropRateDown5);
		items.put(9 + 3 + 6, flintDropRateDown10);
		
		items.put(5, appleDropRate);
		items.put(9 + 5, flintDropRate);
		
		Item horse = new Item(Material.SADDLE, "�6Apparition de cheval �8("+(RulesManager.HORSE ? "�aActiv�e" : "�cD�sactiv�e")+"�8)", null, new ItemHandler() {
			@Override
			public void handle(Player player) {
				RulesManager.HORSE = !RulesManager.HORSE;
				if(!RulesManager.HORSE) {
					for(Entity entitys : Bukkit.getWorld("world").getEntities()) {
						if(entitys instanceof Horse) {
							entitys.remove();
						}
					}
				}
				Bukkit.broadcastMessage(GameManager.PREFIX + "�6L'apparition de cheval est "+(RulesManager.HORSE ? "�aactiv�e" : "�cd�sactiv�e")+"�6.");
			}
		});
		
		Item nether = new Item(Material.NETHERRACK, "�6Acc�s au nether �8("+(RulesManager.NETHER ? "�aActiv�" : "�cD�sactiv�")+"�8)", null, new ItemHandler() {
			@Override
			public void handle(Player player) {
				RulesManager.NETHER = !RulesManager.NETHER;
				Bukkit.broadcastMessage(GameManager.PREFIX + "�6L'acc�s au nether est "+(RulesManager.NETHER ? "�aactiv�e" : "�cd�sactiv�e")+"�6.");
			}
		});
		
		Item goldenHead = new Item(Material.GOLDEN_APPLE, "�6GoldenHead �8("+(RulesManager.GOLDENHEAD ? "�aActiv�e" : "�cD�sactiv�e")+"�8)", null, new ItemHandler() {
			@Override
			public void handle(Player player) {
				RulesManager.GOLDENHEAD = !RulesManager.GOLDENHEAD;
				Bukkit.broadcastMessage(GameManager.PREFIX + "�6Les GoldenHead sont "+(RulesManager.GOLDENHEAD ? "�aactiv�es" : "�cd�sactiv�es")+"�6.");
			}
		});

		items.put(9 * 2 + 4, nether);
		items.put(9 * 2 + 5, goldenHead);
		items.put(9 * 2 + 6, horse);
		
		Item backToMain = new Item(Material.PAPER, "�cRetour au menu principal",
				null, new ItemHandler() {
					@Override
					public void handle(Player player) {
						player.closeInventory();
						new GuiMain().open(player);
					}
				});
		
		items.put(27, backToMain);

		org.bukkit.inventory.InventoryView invOpen = player.getOpenInventory();

		for (Integer slot : items.keySet()) {
			slot--;
			invOpen.setItem(slot, items.get(slot + 1).toItemStack());
		}
	}

	public Inventory getGUI() {
		return GUI;
	}

	@Override
	public void open(Player player) {
		player.closeInventory();
		player.openInventory(GUI.toInventory());
		if(player.getOpenInventory().getTitle().equals(GUI.getTitle())) {
			refresh(player);	
		}
	}

	@Override
	public void clickOnItem(Player player, int slot) {
		for (Integer itemsSlot : items.keySet()) {
			Item item = items.get(itemsSlot);
			if (slot == itemsSlot - 1) {
				item.handle(player);
				if(player.getOpenInventory().getTitle().equals(GUI.getTitle())) {
					refresh(player);	
				}
			}
		}
	}
}
