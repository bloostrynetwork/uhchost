package mc.bloostrynetwork.antox11200.inventorys;

import java.util.HashMap;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import mc.bloostrynetwork.antox11200.inventorys.Inventory;
import mc.bloostrynetwork.antox11200.items.Item;

public interface IGui {

	public abstract void open(Player player);
	public abstract void refresh(Player player);
	public abstract void clickOnItem(Player player, int slot);
	
	public abstract Inventory getGUI();

	public static HashMap<Integer, ItemStack> getDefaultItem() {
		HashMap<Integer, ItemStack> items = new HashMap<>();

		Item deco = new Item(Material.STAINED_GLASS_PANE, ".", null, null);

		int i = 1;
		while (i < 11) { //9
			items.put(i - 1, deco.toItemStack());
			i++;
		}

		i = 18;
		while (i < 28) { // 26 
			items.put(i - 1, deco.toItemStack());
			i++;
		}
		return items;
	}
}
