package mc.bloostrynetwork.antox11200.listeners;

import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

import mc.bloostrynetwork.antox11200.manager.game.GameManager;
import mc.bloostrynetwork.antox11200.player.Kill;
import mc.bloostrynetwork.antox11200.player.UPlayer;

public class PlayerDeathListener implements Listener {

	@EventHandler
	public void PlayerDeathEvent_(PlayerDeathEvent e) {
		if (e.getEntity() instanceof Player) {
			e.setKeepInventory(true);

			final Player player = (Player) e.getEntity();
			Player killer = player.getKiller();
			UPlayer uplayer = GameManager.players.get(player.getName());

			player.setHealth(player.getMaxHealth());

			String reason = "null";

			if (killer instanceof Player && killer != null) {
				UPlayer ukiller = GameManager.players.get(killer.getName());

				if (GameManager.hasTeam())
					reason = uplayer.team.getPrefix() + " " + player.getName() + " �6� �t� tu� par �c"
							+ killer.getName() + " ";
				else
					reason = "�e" + player.getName() + " �6� �t� tu� par �c" + killer.getName() + " ";

				ukiller.addKill(new Kill(killer, player, reason));
			} else {
				if (GameManager.hasTeam())
					reason = e.getDeathMessage().replace(player.getName(),
							uplayer.team.getPrefix() + " " + player.getName()) + " �6";
				else {
					reason = e.getDeathMessage().replace(player.getName(), "�e" + player.getName()) + "�6";
					if (killer != null) {
						reason = reason.replace(killer.getName(), "�e" + killer.getName()+"�6");
					}
				}
			}

			e.setDeathMessage(null);

			uplayer.death(reason);

			((CraftPlayer) player).getHandle().killer = null;
		}
	}
}
