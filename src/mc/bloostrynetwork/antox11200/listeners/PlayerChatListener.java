package mc.bloostrynetwork.antox11200.listeners;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import mc.bloostrynetwork.antox11200.bloostryapi.BloostryAPI;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.player.BloostryPlayer;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.rank.BloostryRank;
import mc.bloostrynetwork.antox11200.manager.game.GameManager;
import mc.bloostrynetwork.antox11200.player.UPlayer;

public class PlayerChatListener implements Listener {

	@EventHandler(priority = EventPriority.HIGHEST)
	public void PlayerChatEvent_(AsyncPlayerChatEvent e) {
		if (e.isCancelled())
			return;

		Player player = e.getPlayer();

		String message = e.getMessage();

		UPlayer uplayer = GameManager.players.get(player.getName());
		if (uplayer.team != null) {
			if (!(message.split(" ")[0].contains("!"))) {
				
				for (String playersName : uplayer.team.getPlayers()) {
					Player players = Bukkit.getPlayer(playersName);
					if(players != null && player.isOnline()) {
						players.sendMessage(GameManager.PREFIX_TEAM+uplayer.team.getPrefix()+ " "+player.getName()+"�7: �f"+message);
					}
				}
				e.setCancelled(true);
			} else {
				String newMessage = "";
				
				newMessage = message.split(" ")[0].replace('!', ' ');
				
				for(int i = 1; i < message.split(" ").length; i++) {
					newMessage += " " +message.split(" ")[i]; 
				}

				BloostryPlayer iplayer = BloostryAPI.getBloostryPlayer(player.getUniqueId());
				BloostryRank rank = iplayer.getBloostryRank();
				e.setFormat(rank.getPrefix() + player.getName() + " �7:" + rank.getChatColor() + newMessage);
			}
			return;
		}
	}
}
