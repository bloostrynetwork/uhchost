package mc.bloostrynetwork.antox11200.listeners;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.event.player.PlayerQuitEvent;

import mc.bloostrynetwork.antox11200.bloostryapi.BloostryAPI;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.player.BloostryPlayer;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.rank.Ranks;
import mc.bloostrynetwork.antox11200.manager.game.GameManager;
import mc.bloostrynetwork.antox11200.manager.game.GameState;
import mc.bloostrynetwork.antox11200.manager.game.Step;
import mc.bloostrynetwork.antox11200.player.PlayerScoreboard;
import mc.bloostrynetwork.antox11200.player.UPlayer;

public class PlayerConnectListener implements Listener {

	public List<String> playersWhitelisted = new ArrayList<>();

	@EventHandler(ignoreCancelled = true, priority = EventPriority.LOW)
	public void PlayerLoginEvent_(PlayerLoginEvent e) {
		Player player = e.getPlayer();
		BloostryPlayer iplayer = BloostryAPI.getBloostryPlayer(player.getUniqueId());

		if (GameManager.players.size() >= GameManager.MAX_PLAYERS) {
			e.disallow(Result.KICK_BANNED, "§7Erreur de connexion sur le serveur \n§cLe serveur est complet !");
			return;
		}

		GameState gameState = GameManager.gameState;
		if (gameState.requestRankLevel) {
			if (gameState.stade == Step.INGAME) {
				if (GameManager.players.containsKey(player.getName())
						&& !GameManager.players.get(player.getName()).dead && iplayer.getBloostryRank().getPermissionPower() >= Ranks.VIP.getBloostryRank().getPermissionPower()) {
					return;
				} else {
					if (iplayer.getBloostryRank().getPermissionPower() >= gameState.level)
						return;
					if (!GameManager.players.containsKey(player.getName())) {
						e.disallow(Result.KICK_WHITELIST,
								"§7Erreur de connexion sur le serveur \n" + gameState.errorMessage);
						return;
					} else if (GameManager.players.get(player.getName()).dead) {
						e.disallow(Result.KICK_WHITELIST,
								"§7Erreur de connexion sur le serveur \n§c✖ Vous êtes mort ✖");
						return;
					}
				}
			} else if (gameState.stade == Step.INWAITING) {
				if (!(iplayer.getBloostryRank().getPermissionPower() >= gameState.level)) {
					e.disallow(Result.KICK_WHITELIST,
							"§7Erreur de connexion sur le serveur \n" + gameState.errorMessage);
					return;
				}
			} else {
				if (!(iplayer.getBloostryRank().getPermissionPower() >= gameState.level)) {
					e.disallow(Result.KICK_WHITELIST,
							"§7Erreur de connexion sur le serveur \n" + gameState.errorMessage);
					return;
				}
			}
		}

	}

	@EventHandler(ignoreCancelled = true, priority = EventPriority.LOW)
	public void PlayerJoinEvent_(PlayerJoinEvent e) {
		Player player = e.getPlayer();
		BloostryPlayer iplayer = BloostryAPI.getBloostryPlayer(player.getUniqueId());

		e.setJoinMessage(null);

		GameState gameState = GameManager.gameState;
		if (gameState.stade == Step.INWAITING) {
			if(GameManager.players.get(player.getName()) == null) {
				GameManager.players.put(player.getName(),
						new UPlayer(player.getName(), player.getUniqueId(), iplayer));
			}
			UPlayer uPlayer = GameManager.players.get(player.getName());
			uPlayer.playerScoreboard = new PlayerScoreboard(uPlayer);
			uPlayer.playerScoreboard.create();
			uPlayer.joinInWaiting();
			e.setJoinMessage(GameManager.PREFIX + "§e" + player.getName() + " §6rejoint la partie §8(§e"
					+ Bukkit.getOnlinePlayers().size() + "§7/§e" + GameManager.MAX_PLAYERS + "§8)§e.");
		} else {
			if (gameState.stade == Step.INGAME) {
				if (gameState == GameState.FINISH) {
					GameManager.showHologramKill(player);
				}
				if (!GameManager.players.containsKey(player.getName())) {
					if (iplayer.getBloostryRank().getPermissionPower() >= gameState.level) {
						GameManager.players.put(player.getName(), new UPlayer(player.getName(), player.getUniqueId(),
								iplayer));
						UPlayer uplayer = GameManager.players.get(player.getName());
						uplayer.connected = true;
						uplayer.setPlayer(player);
						if(uplayer.revive) {
							GameManager.playersInGame.add(player.getName());
							
							uplayer.revive = false;
							player.getInventory().setArmorContents(uplayer.lastEquipments);
							player.getInventory().setContents(uplayer.lastInventory);
							player.teleport(uplayer.lastLocation);
							
							player.setGameMode(GameMode.SURVIVAL);
							
							uplayer.lastInventory = null;
							uplayer.lastEquipments = null;
							uplayer.lastLocation = null;

							player.sendMessage(GameManager.PREFIX+"§aVous avez résister.");
						} else {
							uplayer.joinInStaff();
							player.sendMessage(GameManager.PREFIX + "§6Connexion en tant que spectateur !");
							uplayer.sendTitle("§aConnexion réussie", "§6Vous êtes spectateur", 2);	
						}

						uplayer.playerScoreboard = new PlayerScoreboard(uplayer);
						uplayer.playerScoreboard.create();
					}
					return;
				} else {
					UPlayer uplayer = GameManager.players.get(player.getName());
					if(uplayer.revive) {
						GameManager.playersInGame.add(player.getName());
						
						uplayer.revive = false;
						player.getInventory().setArmorContents(uplayer.lastEquipments);
						player.getInventory().setContents(uplayer.lastInventory);
						player.teleport(uplayer.lastLocation);
						
						player.setGameMode(GameMode.SURVIVAL);
						
						uplayer.lastInventory = null;
						uplayer.lastEquipments = null;
						uplayer.lastLocation = null;
						player.sendMessage(GameManager.PREFIX+"§aVous avez résister.");
					}
					if (uplayer.dead) {
						if (iplayer.getBloostryRank().getPermissionPower() >= gameState.level) {
							uplayer.connected = true;
							uplayer.setPlayer(player);
							uplayer.joinInStaff();
							player.sendMessage(GameManager.PREFIX + "§6Connexion en tant que spectateur !");
							uplayer.sendTitle("§aConnexion réussie", "§6Vous êtes spectateur", 2);
						}
						return;
					}

					uplayer.playerScoreboard.refresh = true;

					if (uplayer.specMode)
						return;
					if (!uplayer.connected) {
						uplayer.connected = true;

						GameManager.playersInGame.add(uplayer.getPlayer().getName());
						e.setJoinMessage(GameManager.PREFIX + "§e" + player.getName() + " §6vient de se reconnecter.");
					}
				}
			}
		}
	}
	
	@EventHandler(ignoreCancelled = true, priority = EventPriority.LOW)
	public void PlayerQuitEvent_(PlayerQuitEvent e) {
		Player player = e.getPlayer();

		e.setQuitMessage(null);

		UPlayer uplayer = GameManager.players.get(player.getName());

		uplayer.playerScoreboard.delete();
		uplayer.connected = false;

		uplayer.lastLocation = player.getLocation();
		uplayer.lastEquipments = player.getInventory().getArmorContents();
		uplayer.lastInventory = player.getInventory().getContents();

		GameState gameState = GameManager.gameState;
		if (gameState.stade == Step.INWAITING) {
			// On retire la personne car il quitte le jeu
			e.setQuitMessage(GameManager.PREFIX + "§e" + player.getName() + " §6quitte la partie §8(§e"
					+ (Bukkit.getOnlinePlayers().size() - 1) + "§7/§e" + GameManager.MAX_PLAYERS + "§8)§e.");
		} else if (gameState.stade == Step.INGAME) {
			if (uplayer.specMode) 
				return;
			if(GameManager.gameState == GameState.TELEPORTING) {
				GameManager.players.remove(player.getName());
				return;
			}
			if (!GameManager.PVP) {
				if (GameManager.gameState == GameState.FINISH)
					return;
				if (uplayer.dead)
					return;
				GameManager.playersInGame.remove(uplayer.getPlayer().getName());
				e.setQuitMessage(GameManager.PREFIX + "§e" + player.getName() + " §6vient de se déconnecter.");
				if (GameManager.playersInGame.size() <= 1) {
					uplayer.death("§e" + player.getName() + " §6est mort en déconnectant.");
					GameManager.gameFinish(GameManager.playersInGame.get(0));
				}
			} else {
				if (GameManager.gameState == GameState.FINISH)
					return;
				if (uplayer.dead)
					return;
				uplayer.death("§e" + player.getName() + " §6est mort en déconnectant.");
			}
		}
	}
}
