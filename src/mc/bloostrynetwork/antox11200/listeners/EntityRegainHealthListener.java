package mc.bloostrynetwork.antox11200.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.event.entity.EntityRegainHealthEvent.RegainReason;

public class EntityRegainHealthListener implements Listener {
	
    @EventHandler
    public void regainHealth(EntityRegainHealthEvent e){
        if(e.getEntity() instanceof Player){
            if(e.getRegainReason() == RegainReason.EATING){
                e.setCancelled(true);
            }
        }
    }
}
