package mc.bloostrynetwork.antox11200.listeners;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntitySpawnEvent;

import mc.bloostrynetwork.antox11200.manager.game.GameManager;
import mc.bloostrynetwork.antox11200.manager.game.Step;

public class EntitySpawnListener implements Listener {

	@EventHandler(priority = EventPriority.LOWEST)
	public void EntitySpawnEvent_(EntitySpawnEvent e) {
		Entity entity = e.getEntity();

		if (GameManager.gameState.stade == Step.INWAITING) {
			if (!(entity instanceof Player)) {
				e.setCancelled(true);
			}
		}
	}
}
