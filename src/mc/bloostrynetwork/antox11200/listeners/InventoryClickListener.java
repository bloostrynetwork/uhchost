package mc.bloostrynetwork.antox11200.listeners;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import mc.bloostrynetwork.antox11200.inventorys.GuiMain;
import mc.bloostrynetwork.antox11200.inventorys.IGui;
import mc.bloostrynetwork.antox11200.inventorys.config.GuiPropertiesGame;
import mc.bloostrynetwork.antox11200.inventorys.rules.GuiRulesConfigueration;
import mc.bloostrynetwork.antox11200.inventorys.scenarios.GuiEditScenarios;

public class InventoryClickListener implements Listener {

	@EventHandler(ignoreCancelled = true, priority = EventPriority.LOWEST)
	public void InventoryClickEvent_(InventoryClickEvent e) {
		Inventory inv = e.getInventory();
		Player player = (Player) e.getWhoClicked();

		List<IGui> guis = new ArrayList<>();
		guis.add(new GuiMain());
		guis.add(new GuiEditScenarios());
		guis.add(new GuiPropertiesGame());
		guis.add(new GuiRulesConfigueration());

		for (IGui gui : guis) {
			if (inv.getName().equals(gui.getGUI().getTitle())) {
				e.setCancelled(true);
				ItemStack itemStack = e.getCurrentItem();
				if (itemStack != null && itemStack.hasItemMeta()) {
					gui.clickOnItem(player, e.getSlot());
				}
			}
		}
	}
}
