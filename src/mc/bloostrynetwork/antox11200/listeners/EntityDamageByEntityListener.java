package mc.bloostrynetwork.antox11200.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;

import mc.bloostrynetwork.antox11200.manager.game.GameManager;
import mc.bloostrynetwork.antox11200.manager.game.GameState;
import mc.bloostrynetwork.antox11200.manager.game.Step;

public class EntityDamageByEntityListener implements Listener {

	@EventHandler
	public void EntityDamageEvent_(EntityDamageEvent e) {
		if (GameManager.gameState == GameState.FINISH) {
			if (e.getEntity() instanceof Player) {
				e.setCancelled(true);
			}
			return;
		}
		if (GameManager.gameState.stade == Step.INWAITING) {
			if (e.getEntity() instanceof Player) {
				e.setCancelled(true);
			}
			return;
		}
		if (e.getEntity() instanceof Player) {
			if (GameManager.gameState.stade == Step.INWAITING) {
				e.setCancelled(true);
			} else if (GameManager.gameState.stade == Step.INGAME) {
				if (!GameManager.DAMAGE)
					e.setCancelled(true);
			}
		}
	}

	@EventHandler
	public void EntityDamageByEntityEvent_(EntityDamageByEntityEvent e) {
		if (GameManager.gameState == GameState.FINISH) {
			if (e.getEntity() instanceof Player) {
				e.setCancelled(true);
			}
			return;
		}
		if (GameManager.gameState.stade == Step.INWAITING) {
			if (e.getEntity() instanceof Player) {
				e.setCancelled(true);
			}
			return;
		}
		if (e.getEntity() instanceof Player) {
			if (GameManager.gameState.stade == Step.INWAITING) {
				e.setCancelled(true);
			} else if (GameManager.gameState.stade == Step.INGAME) {
				if (!GameManager.DAMAGE)
					e.setCancelled(true);
			}
		}
		
		if (e.getEntity() instanceof Player && (e.getDamager() instanceof Player)) {
			if (!GameManager.PVP) {
				e.setCancelled(true);
				e.getDamager().sendMessage(GameManager.PREFIX + "�cLe PvP n'est pas activ� !");
			}
		}
	}
}
