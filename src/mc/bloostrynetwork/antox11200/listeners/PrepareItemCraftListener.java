package mc.bloostrynetwork.antox11200.listeners;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.inventory.CraftingInventory;
import org.bukkit.inventory.ItemStack;

import mc.bloostrynetwork.antox11200.manager.game.GameManager;
import mc.bloostrynetwork.antox11200.manager.game.rules.RulesManager;

public class PrepareItemCraftListener implements Listener {

	@EventHandler
	public void PrepareItemCraftEvent_(PrepareItemCraftEvent e) {
		ItemStack result = e.getRecipe().getResult();
		CraftingInventory inv = (CraftingInventory) e.getInventory();

		if (result.hasItemMeta() && result.getItemMeta().hasDisplayName()
				&& result.getItemMeta().getDisplayName().equalsIgnoreCase("�6Golden Head"))
			if (!RulesManager.GOLDENHEAD) {
				inv.setResult(new ItemStack(Material.AIR));
				e.getView().getPlayer().sendMessage(GameManager.PREFIX + "�cVous ne pouvez pas craft de �fGolden Head�c.");
			}
	}
}
