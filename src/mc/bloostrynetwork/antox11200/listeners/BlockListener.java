package mc.bloostrynetwork.antox11200.listeners;

import org.bukkit.GameMode;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

import mc.bloostrynetwork.antox11200.manager.game.GameManager;
import mc.bloostrynetwork.antox11200.manager.game.GameState;
import mc.bloostrynetwork.antox11200.manager.game.Step;

public class BlockListener implements Listener {

	@EventHandler(priority = EventPriority.LOWEST)
	public void BlockPlaceEvent_(BlockPlaceEvent e) {
		if(GameManager.gameState == GameState.TELEPORTING) {
			e.setCancelled(true);
		}
		if(e.getPlayer().getGameMode() == GameMode.CREATIVE)return;
		if(GameManager.gameState.stade == Step.INWAITING) {
			e.setCancelled(true);
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void BlockBreakEvent_(BlockBreakEvent e) {
		if(GameManager.gameState == GameState.TELEPORTING) {
			e.setCancelled(true);
		}
		if(e.getPlayer().getGameMode() == GameMode.CREATIVE)return;
		if(GameManager.gameState.stade == Step.INWAITING) {
			e.setCancelled(true);
		}
	}
}
